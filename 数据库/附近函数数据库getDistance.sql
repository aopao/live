DELIMITER $$
CREATE FUNCTION `getDistance` (lat1 FLOAT, lon1 FLOAT, lat2 FLOAT, lon2 FLOAT) 
RETURNS float
DETERMINISTIC
BEGIN
RETURN ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN((lat1 * PI() / 180 - lat2 * PI() / 180) / 2), 2)
  + COS(lat1 * PI() / 180) * COS(lat2 * PI() / 180)
  * POW(SIN(( lon1 * PI() / 180 - lon2 * PI() / 180 ) / 2),2))),2);
END$$
DELIMITER ;