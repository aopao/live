<?php

namespace app\admin\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\admin\model\SlideItemModel;

class AdvideoController extends AdminBaseController
{
    public function index()
    {
        $lists = Db::name("video")
            ->where('is_ad=1')
            ->order("id asc")
            ->paginate(20);

        $this->assign('lists', $lists);

        return $this->fetch();

    }
    
        public function add(){
        return $this -> fetch();
    }  


    public function addpost(){
        if ($this->request->isPost()) {

            $data      = $this->request->param();

            // $date['thumb_s'] = $date['thumb'];
            // $date['href_w'] = $date['href'];
            // $date['steps'] = 0;
            // $date['addtime'] = time();
            // $date['isdel'] = 0;
            // $date['music_id'] =0;
            // $date['nopass_time'] =0;
            // $date['watch_ok'] =0;
            // $date['is_ad'] =1;
            // $date['ad_endtime'] = 0;
            // $date['goodsid'] =0;
            // $date['classid'] =0;
           

            $id = DB::name('video')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }

            $action="添加广告视频：{$id}";
            setAdminLog($action);

//            $this->resetcache();
            $this->success("添加成功！");

        }
    }

    public function delete(){

        $id = $this->request->param('id', 0, 'intval');

        $rs = DB::name('video')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }

        $action="删除广告视频：{$id}";
        setAdminLog($action);

        // $this->resetcache();
        $this->success("删除成功！",url("notice/index"));

    }
    
}




