<?php

/**
 * 店铺申请
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class ShopapplyController extends AdminbaseController {
    protected function getStatus($k=''){
        $status=array(
            '0'=>'待处理',
            '1'=>'审核成功',
            '2'=>'审核失败',
        );
        if($k==''){
            return $status;
        }
        return isset($status[$k])?$status[$k]:'';
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $map[]=['status','=',$status];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['name','like','%'.$keyword.'%'];
        }
			

    	$lists = Db::name("shop_apply")
                ->where($map)
                ->order("addtime DESC")
                ->paginate(20);
                
        $lists->each(function($v,$k){
			$v['thumb']=get_upload_path($v['thumb']);
            $v['userinfo']= getUserInfo($v['uid']);
            $v['tel']= m_s($v['uid']);
            return $v;           
        });
                
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        
    	$this->assign("status", $this->getStatus());
    	
    	return $this->fetch();			
    }
    
	function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('shop_apply')->where("uid={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $this->success("删除成功！",url("shopapply/index"));
            
	}
	
    
    function edit(){
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('shop_apply')
            ->where("uid={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['thumb']= get_upload_path($data['thumb']);
        $data['certificate']= get_upload_path($data['certificate']);
        $data['license']= get_upload_path($data['license']);
        $data['other']= get_upload_path($data['other']);
        $data['userinfo']= getUserInfo($data['uid']);
        
        $this->assign('data', $data);
        
        $this->assign("status", $this->getStatus());
        
        return $this->fetch();
        
    }
    
	function editPost(){
		if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            $data['uptime']=time();
			$rs = DB::name('shop_apply')->update($data);
            if($rs===false){
                $this->error("修改失败！");
            }
            
            $this->success("修改成功！");
		}
	}    
}
