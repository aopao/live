<?php

//来来1号店
//QQ：125050230
namespace app\admin\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\admin\model\SlideItemModel;

class GameclassController extends AdminBaseController
{
    public function index()
    {
        $lists = Db::name("gameclass")
            ->where('id<1000')
            ->order("id asc")
            ->paginate(20);

        $this->assign('lists', $lists);

        return $this->fetch();

    }
    
        public function add(){
        return $this -> fetch();
    }
    
   public function addpost(){
        if ($this->request->isPost()) {

            $data      = $this->request->param();



            $id = DB::name('gameclass')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }

            $action="添加首页分类：{$id}";
            setAdminLog($action);

//            $this->resetcache();
            $this->success("添加成功！");

        }
    }

    public function delete(){

        $id = $this->request->param('id', 0, 'intval');

        $rs = DB::name('gameclass')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }

        $action="删除首页分类：{$id}";
        setAdminLog($action);

        // $this->resetcache();
        $this->success("删除成功！",url("gameclass/index"));

    }

}