<?php

/**
 * 商品
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class ShopgoodsController extends AdminbaseController {
    
    protected function getTypes($k=''){
        $type=[
            '0'=>'链接',
            '1'=>'小程序',
        ];
        if($k==''){
            return $type;
        }
        return isset($type[$k])?$type[$k]:'';
    }
    
    protected function getStatus($k=''){
        $status=[
            '-2'=>'管理员下架',
            '-1'=>'下架',
            '0'=>'审核中',
            '1'=>'通过',
            '2'=>'拒绝',
        ];
        if($k==''){
            return $status;
        }
        return isset($status[$k])?$status[$k]:'';
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_coin=isset($data['start_coin']) ? $data['start_coin']: '';
        $end_coin=isset($data['end_coin']) ? $data['end_coin']: '';
        
        if($start_coin!=""){
           $map[]=['price','>=',strtotime($start_coin)];
        }

        if($end_coin!=""){
           $map[]=['price','<=',strtotime($end_coin) + 60*60*24];
        }
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['name','like','%'.$keyword.'%'];
        }
			

    	$lists = Db::name("shop_goods")
                ->where($map)
                ->order("id DESC")
                ->paginate(20);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
			$v['thumb']=get_upload_path($v['thumb']);
            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        
        $this->assign('type', $this->getTypes());
        
    	$this->assign('status', $this->getStatus());
    	
    	return $this->fetch();
    }
    
    function setStatus(){
        
        $id = $this->request->param('id', 0, 'intval');
        $status = $this->request->param('status', 0, 'intval');
        
        $rs = DB::name('shop_goods')->where("id={$id}")->update(['status'=>$status]);
        if(!$rs){
            $this->error("操作失败！");
        }
        
        $this->success("操作成功！");
    }
    
    
    function setRecom(){
        
        $id = $this->request->param('id', 0, 'intval');
        $isrecom = $this->request->param('isrecom', 0, 'intval');
        
        $rs = DB::name('shop_goods')->where("id={$id}")->update(['isrecom'=>$isrecom]);
        if(!$rs){
            $this->error("操作失败！");
        }
        
        $this->success("操作成功！");
    }
		
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('shop_goods')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $this->success("删除成功！",url("shopgoods/index"));
    }
    
}
