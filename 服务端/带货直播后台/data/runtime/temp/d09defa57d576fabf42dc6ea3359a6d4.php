<?php /*a:1:{s:67:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/level/index.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
	<head>
		
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

		<title>我的等级</title>
		<link href='/static/appapi/css/level.css?t=1561712925' rel="stylesheet" type="text/css" >
	</head>
<body >

	<div class="main">
        <div class="tab clearfix">
            <ul>
                <li class="on">
                    用户等级
                    <div class="tab_line bg_default"></div>
                </li>
                <li>
                    主播等级
                    <div class="tab_line bg_default"></div>
                </li>
            </ul>
        </div>
        <div class="line10"></div>
		<div class="top-body ">
			<div class="level">
				<div class="speed">
					<div class="info">
						<div class="name">
                            <img src="http://tupian.daweia.cn/user/20200414/209a8941a40a608961d3c1646ead1670.jpg">
                        </div>
                        <div class="avatar_bg"></div>
						<div class="tip">Lv.1</div>
					</div>
					<div class="level_val">
						经验值：<span id="total" style="margin-right: 10px;">0</span>
													距离升级：<span id="next_diff">50</span>
											</div>
                    <div class="level_jindu">
                        <div class="level_jindu_l">
                            Lv.1                        </div>
                        <div class="jindu">
                            <div class="levelnp">
                                <div class="leveln n2 bg_default" id="progress" style="width:0%; border-radius: 4px;"></div>
                            </div>
                        </div>
                        <div class="level_jindu_r">
                                                            Lv.2                        </div>
                    </div>
					
				</div>
			</div>
			<div class="line10"></div>
			<div class="privilege clearfix">
				<div class="title">等级特权</div>
				<ul>
					<li>
                        <img src="/static/appapi/images/level/v_level_id.png">
                        <div class="privilege_t">排名靠前</div>
                    </li>
					<li>
                        <img src="/static/appapi/images/level/v_level_id2.png">
                        <div class="privilege_t">等级勋章</div>
                    </li>
				</ul>
			</div>
			<div class="line10"></div>
			<div class="upgrade">
				<div class="title">如何升级</div>
				<div class="body">
					<img src="/static/appapi/images/level/v_up.png">
				</div>
			</div>
            <div class="line10"></div>
			<div class="tips">
				<a href="/appapi/level/level">用户等级说明</a>
			</div>
		
		</div>
		
		<div class="top-body hide">
			<div class="level level_a">
				<div class="speed">
                    <div class="info">
						<div class="name">
                            <img src="http://tupian.daweia.cn/user/20200414/209a8941a40a608961d3c1646ead1670.jpg">
                        </div>
                        <div class="avatar_bg"></div>
						<div class="tip">Lv.7</div>
					</div>
					<div class="level_val">
						经验值：<span id="total" style="margin-right: 10px;">25185</span>
													距离升级：<span id="next_diff">4815</span>
											</div>
                    <div class="level_jindu">
                        <div class="level_jindu_l">
                            Lv.7                        </div>
                        <div class="jindu">
                            <div class="levelnp">
                                <div class="leveln n2 bg_default" id="progress" style="width:83%; border-radius: 4px;"></div>
                            </div>
                        </div>
                        <div class="level_jindu_r">
                                                            Lv.8                        </div>
                    </div>

				</div>
			</div>
			<div class="line10"></div>
			<div class="privilege clearfix">
				<div class="title">等级特权</div>
				<ul>
					<li>
                        <img src="/static/appapi/images/level/m_level_id.png">
                        <div class="privilege_t">等级标识</div>
                    </li>
					<li>
                        <img src="/static/appapi/images/level/m_level_id2.png">
                        <div class="privilege_t">身份标识</div>
                    </li>
				</ul>
			</div>
			<div class="line10"></div>
			<div class="upgrade">
				<div class="title">如何升级</div>
				<div class="body">
					<img src="/static/appapi/images/level/m_up.png">
				</div>
			</div>	
            <div class="line10"></div>
			<div class="tips">
				<a href="/appapi/level/level_a">主播等级说明</a>
			</div>
		</div>

	</div> 
</body>

<script>
    var uid='';
    var token='';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


<script>
	(function(){
		$(".tab ul li").on("click",function(){
			$(this).siblings().removeClass("on");
			$(this).addClass("on");
			$(".top-body").hide().eq($(this).index()).show();
		})
	})()
</script>
</body>
</html>