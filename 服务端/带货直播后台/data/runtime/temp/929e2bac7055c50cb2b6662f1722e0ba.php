<?php /*a:3:{s:69:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/family/detail.html";i:1579317638;s:60:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/head.html";i:1579317638;s:62:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/footer.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/family.css"/>
    <title>家族主页</title>
</head>
<body class="detail">
	<div class="detai_top">
		<div class="user-list user-list-fillet">
			<ul>
				<li>
					<div class="thumb">
						<img src="<?php echo $familyinfo['badge']; ?>">
					</div>
					<div class="info">
						<p class="info-title"><?php echo $familyinfo['name']; ?></p>
						<p class="info-des">族长：<?php echo $familyinfo['userinfo']['user_nicename']; ?></p>
						<p class="info-des2">ID：<?php echo $familyinfo['id']; ?></p>
					</div>
					<div class="action">
					</div>
				</li>

			</ul>
		</div>
	</div>
    <div class="line10"></div>
	<div class="des">
		<div class="des_title">简介</div>
		<div class="des_body"><?php echo $familyinfo['briefing']; ?></div>
	</div>
	<div class="line10"></div>
	<div class="anchor">
		<div class="anchor_t">
			签约主播 <span>(<?php echo $familyinfo['count']; ?>)</span>
		</div>
		<div class="user-list">
			<ul>
			    <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				<li>
					<div class="thumb">
						<img src="<?php echo $v['userinfo']['avatar']; ?>">
					</div>
					<div class="info">
						<p class="info-title">
                            <?php echo $v['userinfo']['user_nicename']; ?>
                            <img src="/static/appapi/images/<?php if($v['userinfo']['sex'] == 1): ?>man<?php else: ?>woman<?php endif; ?>.png" class="sex"> 
                            <img src="<?php echo $levellist[$v['userinfo']['level']]['thumb']; ?>" class="level">
                        </p>
						<p class="info-des2">粉丝数：<?php echo $v['userinfo']['fansnum']; ?></p>
					</div>
					<div class="action">
					</div>
				</li>
				<?php endforeach; endif; else: echo "" ;endif; ?>
			</ul>
		</div>
	</div>
	<?php if($familyinfo['isstatus'] != '0' && $familyinfo['isstatus'] != '2'): ?>
	<a href="/Appapi/Family/detail_sign?familyid=<?php echo $familyinfo['id']; ?>&uid=<?php echo $uid; ?>&token=<?php echo $token; ?>">
		<div class="button_default bottombtn sign">
			我要签约
		</div>
	</a>
	<?php endif; ?>
	<script>
		var familyid='<?php echo $familyinfo['id']; ?>';
	</script>
	<script>
    var uid='<?php echo (isset($uid) && ($uid !== '')?$uid:''); ?>';
    var token='<?php echo (isset($token) && ($token !== '')?$token:''); ?>';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


	<script src="/static/appapi/js/family.js"></script>
	<script>
	$(function(){
		function getlistmore(){
			$.ajax({
				url:'/appapi/Family/detail_more',
				data:{'page':page,'uid':uid},
				type:'post',
				dataType:'json',
				success:function(data){
					if(data.nums>0){
							var nums=data.nums;
							var list=data.data;
							var html='';
							for(var i=0;i<nums;i++){
								html='<li>\
										<div class="thumb">\
											<img src="'+list[i]['userinfo']['avatar']+'">\
										</div>\
										<div class="info">\
											<p class="info-title">'+list[i]['userinfo']['user_nicename']+'</p>\
											<p class="info-des"><span>粉丝数：'+list[i]['userinfo']['fansnum']+'</span></p>\
										</div>\
										<div class="action">\
										</div>\
									</li>';
							}
						
						$(".anchor .user-list ul").append(html);
					}
					
					if(data.isscroll==1){
						page++;
						isscroll=true;
					}
				}
			})
		}
		var page=2; 
		var isscroll=true; 

		$(window).scroll(function(){  
				var srollPos = $(window).scrollTop();    //滚动条距顶部距离(页面超出窗口的高度)  		
				var totalheight = parseFloat($(window).height()) + parseFloat(srollPos);  
				if(($(document).height()-50) <= totalheight  && isscroll) {  
						isscroll=false;
						getlistmore()
				}  
		});  

	})
	</script>
	
</body>
</html>