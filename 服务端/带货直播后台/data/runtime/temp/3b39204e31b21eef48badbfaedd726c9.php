<?php /*a:3:{s:67:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/shop/status.html";i:1579317638;s:60:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/head.html";i:1579317638;s:62:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/footer.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

    <title>开启小店</title>
    <link rel="stylesheet" type="text/css" href="/static/appapi/css/auth_status.css?t=1565083698">

</head>
<body>
    <div class="auth_status">
        <div class="status_img">
            <img src="/static/appapi/images/auth_status.png">
        </div>
        
        <div class="status_info">
            <?php if($info && $info['status'] == 2): ?>
            <p class="status_info_t no"><img src="/static/appapi/images/auth_error.png">审核未通过</p>
            <p class="status_info_d"><?php echo $info['reason']; ?></p>
            <?php else: ?>
            <p class="status_info_t">信息审核中...</p>
            <p class="status_info_d">3个工作日内会有审核结果，请耐心等待</p>
            <?php endif; ?>
        </div>
        
        <?php if($info && $info['status'] == 2): ?>
        <div class="autharea">
            <a href="/Appapi/shop/index?uid=<?php echo $uid; ?>&token=<?php echo $token; ?>&reset=1">重新认证</a>
        </div>
        <?php endif; ?>
    </div>
	<script>
    var uid='<?php echo (isset($uid) && ($uid !== '')?$uid:''); ?>';
    var token='<?php echo (isset($token) && ($token !== '')?$token:''); ?>';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


</body>
</html>