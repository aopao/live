<?php /*a:3:{s:66:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/shop/apply.html";i:1579317638;s:60:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/head.html";i:1579317638;s:62:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/footer.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/shop.css"/> 
    <title>开启小店</title>
</head>
<body>
    <div class="apply">
        <div class="line_t">
            <span>*</span>店铺图片（用于店铺主页展示）
        </div>
        <div class="line_bd line_bd_u1">
            <ul class="upload clearfix">
                <li class="square">
                    <div class="up_img" data-fileid="up_img1">
                        <?php if($info): ?>
                        <img src="<?php echo $info['thumb1']; ?>">
                        <?php else: ?>
                        <img src="/static/appapi/images/upload.png">
                        <?php endif; ?>
                        <div class="shadd">
                            <div class="progress_bd"><div class="progress_sp"></div></div>
                        </div>
                        <input type="hidden" class="img_input" name="thumb" id="thumb" value="<?php echo (isset($info['thumb']) && ($info['thumb'] !== '')?$info['thumb']:''); ?>">
                        <input type="file" id="up_img1" class="file_input" name="file"  accept="image/*" style="display:none;"/>
                    </div>
                </li>
            </ul>
        </div>
        
        <div class="line_t">
            <span>*</span>店铺名称（最多可输入10个字）
        </div>
        <div class="line_bd line_bd_i">
            <input id="name" maxlength="10" value="<?php echo (isset($info['name']) && ($info['name'] !== '')?$info['name']:''); ?>">
        </div>
        
        <div class="line_t">
            <span>*</span>店铺简介（最多可输入50个字）
        </div>
        <div class="line_bd line_bd_t">
            <textarea id="des" maxlength="50"><?php echo (isset($info['des']) && ($info['des'] !== '')?$info['des']:''); ?></textarea>
        </div>
        
        <div class="line_t hide">
            <span></span>店铺联系方式（店铺联系电话）
        </div>
        <div class="line_bd line_bd_i hide">
            <input id="tel" value="<?php echo (isset($info['tel']) && ($info['tel'] !== '')?$info['tel']:''); ?>">
        </div>

        <div class="line_bd line_bd_u">
            <ul class="upload clearfix">
                <li>
                    <div class="up_img" data-fileid="up_img2">
                        <?php if($info): ?>
                        <img src="<?php echo $info['certificate1']; ?>">
                        <?php else: ?>
                        <img src="/static/appapi/images/upload.png">
                        <?php endif; ?>
                        <div class="shadd">
                            <div class="progress_bd"><div class="progress_sp"></div></div>
                        </div>
                        <input type="hidden" class="img_input" name="certificate" id="certificate" value="<?php echo (isset($info['certificate']) && ($info['certificate'] !== '')?$info['certificate']:''); ?>">
                        <input type="file" id="up_img2" class="file_input" name="file"  accept="image/*" style="display:none;"/>
                    </div>
                    <div class="up_t">
                        营业执照
                    </div>
                </li>
                <li>
                    <div class="up_img" data-fileid="up_img3">
                        <?php if($info): ?>
                        <img src="<?php echo $info['license1']; ?>">
                        <?php else: ?>
                        <img src="/static/appapi/images/upload.png">
                        <?php endif; ?>
                        <div class="shadd">
                            <div class="progress_bd"><div class="progress_sp"></div></div>
                        </div>
                        <input type="hidden" class="img_input" name="license" id="license" value="<?php echo (isset($info['license']) && ($info['license'] !== '')?$info['license']:''); ?>">
                        <input type="file" id="up_img3" class="file_input" name="file"  accept="image/*" style="display:none;"/>
                    </div>
                    <div class="up_t">
                        许可证
                    </div>
                </li>
                <li>
                    <div class="up_img" data-fileid="up_img4">
                        <?php if($info): ?>
                        <img src="<?php echo $info['other1']; ?>">
                        <?php else: ?>
                        <img src="/static/appapi/images/upload.png">
                        <?php endif; ?>
                        <div class="shadd">
                            <div class="progress_bd"><div class="progress_sp"></div></div>
                        </div>
                        <input type="hidden" class="img_input" name="other" id="other" value="<?php echo (isset($info['other']) && ($info['other'] !== '')?$info['other']:''); ?>">
                        <input type="file" id="up_img4" class="file_input" name="file"  accept="image/*" style="display:none;"/>
                    </div>
                    <div class="up_t">
                        其他证件
                    </div>
                </li>
            </ul>
        </div>
        <?php if($info): if($info['status'] != 0): ?>
            <div class="apply_btn apply_ok">
                提交重审
            </div>
            <?php else: ?>
            <div class="apply_btn apply_no">
                信息审核中
            </div>
            <?php endif; else: ?>
        <div class="apply_btn apply_ok">
            申请店铺
        </div>
        <?php endif; ?>
        
    </div>
	<script>
    var uid='<?php echo (isset($uid) && ($uid !== '')?$uid:''); ?>';
    var token='<?php echo (isset($token) && ($token !== '')?$token:''); ?>';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


    <script src="/static/js/ajaxfileupload.js"></script>
    <script src="/static/appapi/js/shop_apply.js"></script>
	
</body>
</html>