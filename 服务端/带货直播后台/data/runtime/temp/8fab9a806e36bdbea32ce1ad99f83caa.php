<?php /*a:0:{}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/family.css?t=1561712925"/>
    <title>我的家族</title>
</head>
<body >
	<div class="home">
        <div class="detai_top">
            <div class="user-list user-list-fillet">
                <ul>
                    <li>
                        <div class="thumb">
                            <img src="http://tupian.daweia.cn/default/20200415/4e93ed4ab0e8244f75c98c0225716a14.jpg">
                        </div>
                        <div class="info">
                            <p class="info-title">幽灵网络</p>
                            <p class="info-des"></p>
                            <p class="info-des2">ID：2</p>
                        </div>
                        <div class="action">
                        </div>
                    </li>

                </ul>
            </div>
        </div>
        <div class="line10"></div>
		<div class="home_info">
			<ul>
				<li>
					<span class="home_info_t">族长</span>
					<span class="home_info_b">大伟</span>
				</li>
								<li>
					<span class="home_info_t">家族默认抽成</span>
					<span class="home_info_b edit_divide color_default"  data-divide="0"><span id="divide_family">0</span>%</span>
				</li>
							</ul>
		</div>
        <div class="des">
            <div class="des_title">家族简介 <a href="/Appapi/Family/setdes?familyid=2&uid=37104&token=5b237e5f68dc93f6534413c1fd5db001"><span class="edit_des"></span></a></div>
            <div class="des_body">官方网站:www.daweia.cn</div>
        </div>
		<div class="line10"></div>
		<div class="family_home clearfix">
			<ul class="family_home_ul">
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/member?familyid=2&uid=37104&token=5b237e5f68dc93f6534413c1fd5db001">
						<img class="family_home_img" src="/static/appapi/images/family/member.png"/><br>
						<span>家族成员</span>
					</a>
				</li>
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/long?familyid=2&uid=37104&token=5b237e5f68dc93f6534413c1fd5db001">
						<img class="family_home_img" src="/static/appapi/images/family/live.png"/><br>
						<span>主播数据</span>
					</a>
				</li>
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/profit?familyid=2&uid=37104&token=5b237e5f68dc93f6534413c1fd5db001">
						<img class="family_home_img" src="/static/appapi/images/family/profit.png"/><br>
						<span>家族盈利</span>
					</a>
				</li>
								<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/examine?familyid=2&uid=37104&token=5b237e5f68dc93f6534413c1fd5db001">
						<img class="family_home_img" src="/static/appapi/images/family/examine.png"/><br>
						<span>签约审核</span>
					</a>
				</li>
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/signout?familyid=2&uid=37104&token=5b237e5f68dc93f6534413c1fd5db001">
						<img class="family_home_img" src="/static/appapi/images/family/quit.png"/><br>
						<span>解约审核</span>
					</a>
				</li>
								
			</ul>
		</div>
	</div>
	<script>
		var familyid='2';
	</script>
	<script>
    var uid='37104';
    var token='5b237e5f68dc93f6534413c1fd5db001';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


	<script src="/static/appapi/js/family.js"></script>
</body>
</html>