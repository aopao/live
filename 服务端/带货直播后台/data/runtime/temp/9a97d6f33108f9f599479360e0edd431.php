<?php /*a:1:{s:66:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/mall/index.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
	<head>
		
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

		<title>在线商城</title>
		<link href='/static/appapi/css/mall.css?t=1576821468' rel="stylesheet" type="text/css" >
	</head>
<body>
    <div class="tab clearfix">
        <ul>
            <li class="on">
                会员
                <div class="tab_line bg_default"></div>
            </li>
            <li>
                靓号
                <div class="tab_line bg_default"></div>
            </li>
            <li>
                坐骑
                <div class="tab_line bg_default"></div>
            </li>
        </ul>
    </div>
    <div class="tab_bd vip">
        <div class="bd_title">
            <span class="bd_title_line">——</span>
            <span class="bd_title_txt">VIP会员专属特权</span>
            <span class="bd_title_line">——</span>
        </div>
        <div class="bd_content clearfix">
            <ul>
                <li>
                    <div class="v_t">尊贵身份</div>
                    <div class="v_d">携带VIP标识登场</div>
                </li>
                <li>
                    <div class="v_t">炫酷标识</div>
                    <div class="v_d">用户聊天显示VIP会员图标</div>
                </li>
            </ul>
        </div>
        <div class="vip_button button_default">
            开通会员
        </div>
        <div class="vip_end hide">
            当前VIP到期时间：<span id="vip_endtime"></span>
        </div>
        
        <div class="vip_buy_body hide">
            <div class="vip_buy_list">
                <div class="vip_buy_list_t">开通账号</div>
                <div class="vip_buy_list_b">手机用户2520 (37128)</div>
            </div>
            <div class="vip_buy_list hide">
                <div class="vip_buy_list_t">会员类型</div>
                <div class="vip_buy_list_b">
                    <ul>
                        <li class="on">VIP会员</li>
                    </ul>
                </div>
            </div>
            <div class="vip_buy_list">
                <div class="vip_buy_list_t vip_length_t">开通时长</div>
                <div class="vip_buy_list_b vip_length">
                    <ul>
                                                <li class="on" data-id="3" data-coin="1000" data-score="1000" data-length="6个月">
                            6个月                        </li>
                                                <li  data-id="2" data-coin="300" data-score="300" data-length="3个月">
                            3个月                        </li>
                                                <li  data-id="1" data-coin="10" data-score="10" data-length="1个月">
                            1个月                        </li>
                                            </ul>
                </div>
            </div>
            <div class="vip_buy_list">
                <div class="vip_buy_list_t">开通花费</div>
                <div class="vip_buy_list_b">
                    <!-- <img src="/static/appapi/images/coin.png" class="coin"> -->
                    <span id="vip_total_coin">1000钻石/1000积分</span>
                </div>
            </div>
            
            <div class="vip_buy_list vip_user">
                <div class="vip_buy_list_t">我的钱包</div>
                <div class="vip_buy_list_b">
                    <!-- <img src="/static/appapi/images/coin.png" class="coin"> -->
                    <span id="user_coin">888888钻石/0积分</span>
                </div>
            </div>
            
            <div class="vip_buy_list vip_submit_bd">
                <div class="vip_submit_bd_b vip_submit_bd_l button_default" data-type='0'>
                    钻石开通                </div>
                <div class="vip_submit_bd_b vip_submit_bd_r" data-type='1'>
                    积分兑换
                </div>
            </div>
        </div>
    </div>
    <div class="tab_bd liang hide">
        <div class="bd_title">
            <span class="bd_title_line">——</span>
            <span class="bd_title_txt">发言时会携带尊贵靓号标识</span>
            <span class="bd_title_line">——</span>
        </div>
        <div class="bd_content clearfix">
            <ul>
                                <li>
                    <div class="liang_id">ID:999999</div>
                    <div class="liang_coin">555钻石</div>
                    <div class="liang_coin">555积分</div>
                    <div class="liang_buy" data-id="3" data-name="999999" data-coin="555钻石" data-score="555积分" >
                        <div class="liang_buy_b liang_buy_l" data-type='0'>
                            钻石购买
                        </div>
                        <div class="liang_buy_b liang_buy_r" data-type='1'>
                            积分兑换
                        </div>
                    </div>
                </li>
                                <li>
                    <div class="liang_id">ID:666666</div>
                    <div class="liang_coin">123钻石</div>
                    <div class="liang_coin">123积分</div>
                    <div class="liang_buy" data-id="2" data-name="666666" data-coin="123钻石" data-score="123积分" >
                        <div class="liang_buy_b liang_buy_l" data-type='0'>
                            钻石购买
                        </div>
                        <div class="liang_buy_b liang_buy_r" data-type='1'>
                            积分兑换
                        </div>
                    </div>
                </li>
                                <li>
                    <div class="liang_id">ID:123456</div>
                    <div class="liang_coin">999钻石</div>
                    <div class="liang_coin">999积分</div>
                    <div class="liang_buy" data-id="1" data-name="123456" data-coin="999钻石" data-score="999积分" >
                        <div class="liang_buy_b liang_buy_l" data-type='0'>
                            钻石购买
                        </div>
                        <div class="liang_buy_b liang_buy_r" data-type='1'>
                            积分兑换
                        </div>
                    </div>
                </li>
                            </ul>
        </div>
    </div>
    <div class="tab_bd car hide">
        <div class="bd_content clearfix">
            <ul>
                                <li>
                    <div class="car_img">
                        <img src="http://tupian.daweia.cn/admin/20200414/bf3589e45275e31ebf04114335d0c70c.png">
                    </div>
                    <div class="car_title">
                        小毛驴                    </div>
                    <div class="car_coin">
                        200钻石/月
                    </div>
                    <div class="car_coin">
                        200积分/月
                    </div>
                    <div class="car_buy" data-id="2" data-name="小毛驴" data-coin="200" data-score="200">
                        <div class="car_buy_b car_buy_l" data-type='0'>
                            钻石购买
                        </div>
                        <div class="car_buy_b car_buy_r" data-type='1'>
                            积分兑换
                        </div>
                    </div>
                </li>
                                <li>
                    <div class="car_img">
                        <img src="http://tupian.daweia.cn/admin/20200414/bc84d83748efa3e6ea8140dcebfe9f1b.png">
                    </div>
                    <div class="car_title">
                        魔法扫把                    </div>
                    <div class="car_coin">
                        300钻石/月
                    </div>
                    <div class="car_coin">
                        300积分/月
                    </div>
                    <div class="car_buy" data-id="5" data-name="魔法扫把" data-coin="300" data-score="300">
                        <div class="car_buy_b car_buy_l" data-type='0'>
                            钻石购买
                        </div>
                        <div class="car_buy_b car_buy_r" data-type='1'>
                            积分兑换
                        </div>
                    </div>
                </li>
                                <li>
                    <div class="car_img">
                        <img src="http://tupian.daweia.cn/admin/20200414/8094610ce35a75e15cd4f4b7587917d1.png">
                    </div>
                    <div class="car_title">
                        小乌龟                    </div>
                    <div class="car_coin">
                        100钻石/月
                    </div>
                    <div class="car_coin">
                        100积分/月
                    </div>
                    <div class="car_buy" data-id="1" data-name="小乌龟" data-coin="100" data-score="100">
                        <div class="car_buy_b car_buy_l" data-type='0'>
                            钻石购买
                        </div>
                        <div class="car_buy_b car_buy_r" data-type='1'>
                            积分兑换
                        </div>
                    </div>
                </li>
                            </ul>
        </div>
    </div>
</body>
<script>
    var uid='37128';
    var token='bfbf74644aa9e9cc5cc619bfd11d9e08';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


<script>
    var vip_txt='开通';
    var name_coin='钻石';
    var name_score='积分';
</script>
<script src="/static/appapi/js/mall.js?t=1578620089"></script>
</body>
</html>