<?php /*a:1:{s:66:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/shop/apply.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/shop.css"/> 
    <title>开启小店</title>
</head>
<body>
    <div class="apply">
        <div class="line_t">
            <span>*</span>店铺图片（用于店铺主页展示）
        </div>
        <div class="line_bd line_bd_u1">
            <ul class="upload clearfix">
                <li class="square">
                    <div class="up_img" data-fileid="up_img1">
                                                <img src="/static/appapi/images/upload.png">
                                                <div class="shadd">
                            <div class="progress_bd"><div class="progress_sp"></div></div>
                        </div>
                        <input type="hidden" class="img_input" name="thumb" id="thumb" value="">
                        <input type="file" id="up_img1" class="file_input" name="file"  accept="image/*" style="display:none;"/>
                    </div>
                </li>
            </ul>
        </div>
        
        <div class="line_t">
            <span>*</span>店铺名称（最多可输入10个字）
        </div>
        <div class="line_bd line_bd_i">
            <input id="name" maxlength="10" value="">
        </div>
        
        <div class="line_t">
            <span>*</span>店铺简介（最多可输入50个字）
        </div>
        <div class="line_bd line_bd_t">
            <textarea id="des" maxlength="50"></textarea>
        </div>
        
        <div class="line_t hide">
            <span></span>店铺联系方式（店铺联系电话）
        </div>
        <div class="line_bd line_bd_i hide">
            <input id="tel" value="">
        </div>

        <div class="line_bd line_bd_u">
            <ul class="upload clearfix">
                <li>
                    <div class="up_img" data-fileid="up_img2">
                                                <img src="/static/appapi/images/upload.png">
                                                <div class="shadd">
                            <div class="progress_bd"><div class="progress_sp"></div></div>
                        </div>
                        <input type="hidden" class="img_input" name="certificate" id="certificate" value="">
                        <input type="file" id="up_img2" class="file_input" name="file"  accept="image/*" style="display:none;"/>
                    </div>
                    <div class="up_t">
                        营业执照
                    </div>
                </li>
                <li>
                    <div class="up_img" data-fileid="up_img3">
                                                <img src="/static/appapi/images/upload.png">
                                                <div class="shadd">
                            <div class="progress_bd"><div class="progress_sp"></div></div>
                        </div>
                        <input type="hidden" class="img_input" name="license" id="license" value="">
                        <input type="file" id="up_img3" class="file_input" name="file"  accept="image/*" style="display:none;"/>
                    </div>
                    <div class="up_t">
                        许可证
                    </div>
                </li>
                <li>
                    <div class="up_img" data-fileid="up_img4">
                                                <img src="/static/appapi/images/upload.png">
                                                <div class="shadd">
                            <div class="progress_bd"><div class="progress_sp"></div></div>
                        </div>
                        <input type="hidden" class="img_input" name="other" id="other" value="">
                        <input type="file" id="up_img4" class="file_input" name="file"  accept="image/*" style="display:none;"/>
                    </div>
                    <div class="up_t">
                        其他证件
                    </div>
                </li>
            </ul>
        </div>
                <div class="apply_btn apply_ok">
            申请店铺
        </div>
                
    </div>
	<script>
    var uid='37128';
    var token='bfbf74644aa9e9cc5cc619bfd11d9e08';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


    <script src="/static/js/ajaxfileupload.js"></script>
    <script src="/static/appapi/js/shop_apply.js"></script>
	
</body>
</html>