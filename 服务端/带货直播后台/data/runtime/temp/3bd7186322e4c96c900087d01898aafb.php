<?php /*a:0:{}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/family.css?t=1561712925"/>
    <title>申请进度</title>
</head>
<body>
	<div class="apply_wait">
		<div class="speed">
			<div class="speed_thumb"><img src="/static/appapi/images/family/create_wait.png"></div>
			<div class="speed_title">等待审核中</div>
			<div class="speed_tips">
				如管理员在10个工作日内未接受审核，<br>视为不通过，可再次发起创建申请
			</div>
			<div class="speed_tips2">
				在此期间您可以先浏览其他家族哦
			</div>
		</div>
		<div class="line10" ></div>
		<form id="apply_form" class="apply_form">
			<div class="term">
				<span class="form_span">家族名称</span>
				<input type="text"  id="name" name="name" class="form_input" readonly value="幽灵网络">
			</div>
			<div class="line" ></div>
			<div class="term">
				<span class="form_span">个人姓名</span>
				<input type="text"  id="fullname" name="fullname" class="form_input" readonly value="大伟">
			</div>
			<div class="line" ></div>
			<div class="term">
				<span class="form_span">身份证号</span>
				<input type="text"  id="carded" name="carded" class="form_input" readonly value="522322198707191213">
			</div>
			<div class="line" ></div>
			<div class="term">
				<span class="form_span sf2">家族简介</span>
				<textarea  type="text"  id="briefing" name="briefing" class="form_textarea" readonly>官方网站:www.daweia.cn</textarea>
			</div>
			<div class="line" ></div>
			<div class="term t">
				<span class="form_span sf2">身份图片</span>
				<img src="http://tupian.daweia.cn/default/20200415/792baa4584629148f2479ee8a49d296a.jpg" class="fl img-sfz" data-index="ipt-file1" id="img_file1" >
				<img src="http://tupian.daweia.cn/default/20200415/da84732f64ee78dc41df708c8c43a90a.jpg" class="fl img-sfz" data-index="ipt-file2" id="img_file2" >
				<div class="cl"></div>
			</div>
			<div class="term t">
				<span class="form_span sf2">家族图片</span>
				<img src="http://tupian.daweia.cn/default/20200415/4e93ed4ab0e8244f75c98c0225716a14.jpg" class="fl img-sfz" data-index="ipt-file3" id="img_file3" >
			</div>
		</form>
		<div class="bottombtn revoke button_default">
			撤销申请
		</div>
	</div>
	<script>
    var uid='37104';
    var token='5b237e5f68dc93f6534413c1fd5db001';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


	<script src="/static/appapi/js/family.js"></script>
</body>
</html>