<?php /*a:3:{s:67:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/family/home.html";i:1579317638;s:60:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/head.html";i:1579317638;s:62:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/footer.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/family.css?t=1561712925"/>
    <title>我的家族</title>
</head>
<body >
	<div class="home">
        <div class="detai_top">
            <div class="user-list user-list-fillet">
                <ul>
                    <li>
                        <div class="thumb">
                            <img src="<?php echo $familyinfo['badge']; ?>">
                        </div>
                        <div class="info">
                            <p class="info-title"><?php echo $familyinfo['name']; ?></p>
                            <p class="info-des"></p>
                            <p class="info-des2">ID：<?php echo $familyinfo['id']; ?></p>
                        </div>
                        <div class="action">
                        </div>
                    </li>

                </ul>
            </div>
        </div>
        <div class="line10"></div>
		<div class="home_info">
			<ul>
				<li>
					<span class="home_info_t">族长</span>
					<span class="home_info_b"><?php echo $familyinfo['userinfo']['user_nicename']; ?></span>
				</li>
				<?php if($type == '1'): ?>
				<li>
					<span class="home_info_t">家族默认抽成</span>
					<span class="home_info_b edit_divide color_default"  data-divide="<?php echo $familyinfo['divide_family']; ?>"><span id="divide_family"><?php echo $familyinfo['divide_family']; ?></span>%</span>
				</li>
				<?php else: ?>
				<li>
					<span class="home_info_t">家族抽成比例</span>
					<span class="home_info_b color_default"><span id="divide_family"><?php echo $divide_family; ?></span>%</span>
				</li>
				<?php endif; ?>
			</ul>
		</div>
        <div class="des">
            <div class="des_title">家族简介 <a href="/Appapi/Family/setdes?familyid=<?php echo $familyinfo['id']; ?>&uid=<?php echo $uid; ?>&token=<?php echo $token; ?>"><span class="edit_des"></span></a></div>
            <div class="des_body"><?php echo $familyinfo['briefing']; ?></div>
        </div>
		<div class="line10"></div>
		<div class="family_home clearfix">
			<ul class="family_home_ul">
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/member?familyid=<?php echo $familyinfo['id']; ?>&uid=<?php echo $uid; ?>&token=<?php echo $token; ?>">
						<img class="family_home_img" src="/static/appapi/images/family/member.png"/><br>
						<span>家族成员</span>
					</a>
				</li>
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/long?familyid=<?php echo $familyinfo['id']; ?>&uid=<?php echo $uid; ?>&token=<?php echo $token; ?>">
						<img class="family_home_img" src="/static/appapi/images/family/live.png"/><br>
						<span>主播数据</span>
					</a>
				</li>
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/profit?familyid=<?php echo $familyinfo['id']; ?>&uid=<?php echo $uid; ?>&token=<?php echo $token; ?>">
						<img class="family_home_img" src="/static/appapi/images/family/profit.png"/><br>
						<span>家族盈利</span>
					</a>
				</li>
				<?php if($type == '1'): ?>
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/examine?familyid=<?php echo $familyinfo['id']; ?>&uid=<?php echo $uid; ?>&token=<?php echo $token; ?>">
						<img class="family_home_img" src="/static/appapi/images/family/examine.png"/><br>
						<span>签约审核</span>
					</a>
				</li>
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/signout?familyid=<?php echo $familyinfo['id']; ?>&uid=<?php echo $uid; ?>&token=<?php echo $token; ?>">
						<img class="family_home_img" src="/static/appapi/images/family/quit.png"/><br>
						<span>解约审核</span>
					</a>
				</li>
				<?php else: ?>
				<li class="family_home_li">
					<a class="family_home_a" href="/Appapi/family/relieve?uid=<?php echo $uid; ?>&token=<?php echo $token; ?>">
						<img class="family_home_img" src="/static/appapi/images/family/relieve.png"/><br>
						<span>解除签约</span>
					</a>
				</li>
				<?php endif; ?>
				
			</ul>
		</div>
	</div>
	<script>
		var familyid='<?php echo $familyinfo['id']; ?>';
	</script>
	<script>
    var uid='<?php echo (isset($uid) && ($uid !== '')?$uid:''); ?>';
    var token='<?php echo (isset($token) && ($token !== '')?$token:''); ?>';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


	<script src="/static/appapi/js/family.js"></script>
</body>
</html>