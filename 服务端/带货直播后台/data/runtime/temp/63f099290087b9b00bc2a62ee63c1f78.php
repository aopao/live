<?php /*a:3:{s:67:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/level/index.html";i:1579317638;s:60:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/head.html";i:1579317638;s:62:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/footer.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
	<head>
		
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

		<title>我的等级</title>
		<link href='/static/appapi/css/level.css?t=1561712925' rel="stylesheet" type="text/css" >
	</head>
<body >

	<div class="main">
        <div class="tab clearfix">
            <ul>
                <li class="on">
                    用户等级
                    <div class="tab_line bg_default"></div>
                </li>
                <li>
                    主播等级
                    <div class="tab_line bg_default"></div>
                </li>
            </ul>
        </div>
        <div class="line10"></div>
		<div class="top-body ">
			<div class="level">
				<div class="speed">
					<div class="info">
						<div class="name">
                            <img src="<?php echo $userinfo['avatar']; ?>">
                        </div>
                        <div class="avatar_bg"></div>
						<div class="tip">Lv.<?php echo $levelinfo['levelid']; ?></div>
					</div>
					<div class="level_val">
						经验值：<span id="total" style="margin-right: 10px;"><?php echo $userinfo['consumption']; ?></span>
						<?php if($type == '1'): ?>
							距离升级：<span id="next_diff"><?php echo $cha; ?></span>
						<?php else: ?>
							你已经达到最高级别
						<?php endif; ?>
					</div>
                    <div class="level_jindu">
                        <div class="level_jindu_l">
                            Lv.<?php echo $levelinfo['levelid']; ?>
                        </div>
                        <div class="jindu">
                            <div class="levelnp">
                                <div class="leveln n2 bg_default" id="progress" style="width:<?php echo $baifen; ?>%; border-radius: 4px;"></div>
                            </div>
                        </div>
                        <div class="level_jindu_r">
                            <?php if($type == '1'): ?>
                                Lv.<?php echo $levelinfo['levelid']+1; else: ?>
                                Lv.Max
                            <?php endif; ?>
                        </div>
                    </div>
					
				</div>
			</div>
			<div class="line10"></div>
			<div class="privilege clearfix">
				<div class="title">等级特权</div>
				<ul>
					<li>
                        <img src="/static/appapi/images/level/v_level_id.png">
                        <div class="privilege_t">排名靠前</div>
                    </li>
					<li>
                        <img src="/static/appapi/images/level/v_level_id2.png">
                        <div class="privilege_t">等级勋章</div>
                    </li>
				</ul>
			</div>
			<div class="line10"></div>
			<div class="upgrade">
				<div class="title">如何升级</div>
				<div class="body">
					<img src="/static/appapi/images/level/v_up.png">
				</div>
			</div>
            <div class="line10"></div>
			<div class="tips">
				<a href="/appapi/level/level">用户等级说明</a>
			</div>
		
		</div>
		
		<div class="top-body hide">
			<div class="level level_a">
				<div class="speed">
                    <div class="info">
						<div class="name">
                            <img src="<?php echo $userinfo['avatar']; ?>">
                        </div>
                        <div class="avatar_bg"></div>
						<div class="tip">Lv.<?php echo $levelinfo_a['levelid']; ?></div>
					</div>
					<div class="level_val">
						经验值：<span id="total" style="margin-right: 10px;"><?php echo $userinfo['votestotal']; ?></span>
						<?php if($type_a == '1'): ?>
							距离升级：<span id="next_diff"><?php echo $cha_a; ?></span>
						<?php else: ?>
							你已经达到最高级别
						<?php endif; ?>
					</div>
                    <div class="level_jindu">
                        <div class="level_jindu_l">
                            Lv.<?php echo $levelinfo_a['levelid']; ?>
                        </div>
                        <div class="jindu">
                            <div class="levelnp">
                                <div class="leveln n2 bg_default" id="progress" style="width:<?php echo $baifen_a; ?>%; border-radius: 4px;"></div>
                            </div>
                        </div>
                        <div class="level_jindu_r">
                            <?php if($type_a == '1'): ?>
                                Lv.<?php echo $levelinfo_a['levelid']+1; else: ?>
                                Lv.Max
                            <?php endif; ?>
                        </div>
                    </div>

				</div>
			</div>
			<div class="line10"></div>
			<div class="privilege clearfix">
				<div class="title">等级特权</div>
				<ul>
					<li>
                        <img src="/static/appapi/images/level/m_level_id.png">
                        <div class="privilege_t">等级标识</div>
                    </li>
					<li>
                        <img src="/static/appapi/images/level/m_level_id2.png">
                        <div class="privilege_t">身份标识</div>
                    </li>
				</ul>
			</div>
			<div class="line10"></div>
			<div class="upgrade">
				<div class="title">如何升级</div>
				<div class="body">
					<img src="/static/appapi/images/level/m_up.png">
				</div>
			</div>	
            <div class="line10"></div>
			<div class="tips">
				<a href="/appapi/level/level_a">主播等级说明</a>
			</div>
		</div>

	</div> 
</body>

<script>
    var uid='<?php echo (isset($uid) && ($uid !== '')?$uid:''); ?>';
    var token='<?php echo (isset($token) && ($token !== '')?$token:''); ?>';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


<script>
	(function(){
		$(".tab ul li").on("click",function(){
			$(this).siblings().removeClass("on");
			$(this).addClass("on");
			$(".top-body").hide().eq($(this).index()).show();
		})
	})()
</script>
</body>
</html>