<?php /*a:2:{s:77:"/www/wwwroot/zhibo.daweia.cn/themes/admin_simpleboot3/admin/setting/site.html";i:1579317638;s:72:"/www/wwwroot/zhibo.daweia.cn/themes/admin_simpleboot3/public/header.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta name="referrer" content="origin">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
.control3{
    display:inline;
    width:300px;
}
</style>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#A" data-toggle="tab"><?php echo lang('WEB_SITE_INFOS'); ?></a></li>
        <li><a href="#B" data-toggle="tab"><?php echo lang('SEO_SETTING'); ?></a></li>
        <li><a href="#C" data-toggle="tab">APP版本管理</a></li>
        <li><a href="#D" data-toggle="tab">登录开关</a></li>
        <li><a href="#E" data-toggle="tab">分享设置</a></li>
        <li><a href="#H" data-toggle="tab">直播管理</a></li>
        <li><a href="#I" data-toggle="tab">美颜/萌颜</a></li>
        <!-- <li><a href="#G" data-toggle="tab">CDN设置</a></li> -->
    </ul>
    <form class="form-horizontal js-ajax-form margin-top-20" role="form" action="<?php echo url('setting/sitePost'); ?>"
          method="post">
        <fieldset>
            <div class="tabbable">
                <div class="tab-content">
                    <div class="tab-pane active" id="A">
                        <div class="form-group">
                            <label for="input-maintain_switch" class="col-sm-2 control-label">网站维护</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="options[maintain_switch]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($site_info['maintain_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                                网站维护开启后，无法开启直播，进入直播间
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-maintain_tips" class="col-sm-2 control-label">维护提示</label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-maintain_tips" name="options[maintain_tips]" ><?php echo (isset($site_info['maintain_tips']) && ($site_info['maintain_tips'] !== '')?$site_info['maintain_tips']:''); ?></textarea> 维护提示信息（200字以内）
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-site-name" class="col-sm-2 control-label"><span
                                    class="form-required">*</span><?php echo lang('WEBSITE_NAME'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site-name" name="options[site_name]"
                                       value="<?php echo (isset($site_info['site_name']) && ($site_info['site_name'] !== '')?$site_info['site_name']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-site" class="col-sm-2 control-label"><span
                                    class="form-required">*</span>网站域名</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site-name" name="options[site]" value="<?php echo (isset($site_info['site']) && ($site_info['site'] !== '')?$site_info['site']:''); ?>">
                                格式： http(s)://xxxx.com(:端口号)
                            </div> 
                        </div>
                        <div class="form-group" style="display:none;">
                            <label for="input-admin_url_password" class="col-sm-2 control-label">
                                后台加密码
                                <a href="http://www.thinkcmf.com/faq.html?url=https://www.kancloud.cn/thinkcmf/faq/493509"
                                   title="查看帮助手册"
                                   data-toggle="tooltip"
                                   target="_blank"><i class="fa fa-question-circle"></i></a>
                            </label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-admin_url_password"
                                       name="admin_settings[admin_password]"
                                       value="<?php echo (isset($admin_settings['admin_password']) && ($admin_settings['admin_password'] !== '')?$admin_settings['admin_password']:''); ?>"
                                       id="js-site-admin-url-password">
                                <p class="help-block">英文字母数字，不能为纯数字</p>
                                <p class="help-block" style="color: red;">
                                    设置加密码后必须通过以下地址访问后台,请劳记此地址，为了安全，您也可以定期更换此加密码!</p>
                                <?php 
                                    $root=cmf_get_root();
                                    $root=empty($root)?'':'/'.$root;
                                    $site_domain = cmf_get_domain().$root;
                                 ?>
                                <p class="help-block">后台登录地址：<span id="js-site-admin-url"><?php echo $site_domain; ?>/<?php echo (isset($admin_settings['admin_password']) && ($admin_settings['admin_password'] !== '')?$admin_settings['admin_password']:'admin'); ?></span>
                                </p>
                            </div>
                        </div>

                        <div class="form-group" style="display:none;">
                            <label for="input-site_admin_theme" class="col-sm-2 control-label">后台模板</label>
                            <div class="col-md-6 col-sm-10">
                                <?php 
                                    $site_admin_theme=empty($admin_settings['admin_theme'])?'':$admin_settings['admin_theme'];
                                 ?>
                                <select class="form-control" name="admin_settings[admin_theme]"
                                        id="input-site_admin_theme">
                                    <?php if(is_array($admin_themes) || $admin_themes instanceof \think\Collection || $admin_themes instanceof \think\Paginator): if( count($admin_themes)==0 ) : echo "" ;else: foreach($admin_themes as $key=>$vo): $admin_theme_selected = $site_admin_theme == $vo ? "selected" : ""; ?>
                                        <option value="<?php echo $vo; ?>" <?php echo $admin_theme_selected; ?>><?php echo $vo; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="display:none;">
                            <label for="input-site_adminstyle" class="col-sm-2 control-label"><?php echo lang('WEBSITE_ADMIN_THEME'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <?php 
                                    $site_admin_style=empty($admin_settings['admin_style'])?cmf_get_admin_style():$admin_settings['admin_style'];
                                 ?>
                                <select class="form-control" name="admin_settings[admin_style]"
                                        id="input-site_adminstyle">
                                    <?php if(is_array($admin_styles) || $admin_styles instanceof \think\Collection || $admin_styles instanceof \think\Paginator): if( count($admin_styles)==0 ) : echo "" ;else: foreach($admin_styles as $key=>$vo): $admin_style_selected = $site_admin_style == $vo ? "selected" : ""; ?>
                                        <option value="<?php echo $vo; ?>" <?php echo $admin_style_selected; ?>><?php echo $vo; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <?php if(APP_DEBUG && false): ?>
                            <div class="form-group">
                                <label for="input-default_app" class="col-sm-2 control-label">默认应用</label>
                                <div class="col-md-6 col-sm-10">
                                    <?php 
                                        $site_default_app=empty($cmf_settings['default_app'])?'demo':$cmf_settings['default_app'];
                                     ?>
                                    <select class="form-control" name="cmf_settings[default_app]"
                                            id="input-default_app">
                                        <?php if(is_array($apps) || $apps instanceof \think\Collection || $apps instanceof \think\Paginator): if( count($apps)==0 ) : echo "" ;else: foreach($apps as $key=>$vo): $default_app_selected = $site_default_app == $vo ? "selected" : "";
                                             ?>
                                            <option value="<?php echo $vo; ?>" <?php echo $default_app_selected; ?>><?php echo $vo; ?></option>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!--
                        <div class="form-group">
                            <label for="input-html_cache_on" class="col-sm-2 control-label"><?php echo lang('HTML_CACHE'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <?php $html_cache_on_checked=empty($html_cache_on)?'':'checked'; ?>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="options[mobile_tpl_enabled]" value="1" id="input-html_cache_on" <?php echo $html_cache_on_checked; ?>></label>
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="form-group">
                            <label for="input-copyright" class="col-sm-2 control-label">版权信息</label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-copyright" name="options[copyright]" ><?php echo (isset($site_info['copyright']) && ($site_info['copyright'] !== '')?$site_info['copyright']:''); ?></textarea> 版权信息（200字以内）
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-name_coin" class="col-sm-2 control-label">钻石名称</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-name_coin" name="options[name_coin]"
                                       value="<?php echo (isset($site_info['name_coin']) && ($site_info['name_coin'] !== '')?$site_info['name_coin']:''); ?>"> 用户充值得到的虚拟币名称
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-name_score" class="col-sm-2 control-label">积分名称</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-name_score" name="options[name_score]"
                                       value="<?php echo (isset($site_info['name_score']) && ($site_info['name_score'] !== '')?$site_info['name_score']:''); ?>"> 直播间玩游戏得到的虚拟币名称
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-name_votes" class="col-sm-2 control-label">映票名称</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-name_votes"
                                       name="options[name_votes]"
                                       value="<?php echo (isset($site_info['name_votes']) && ($site_info['name_votes'] !== '')?$site_info['name_votes']:''); ?>">主播获得的虚拟票名称
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-mobile" class="col-sm-2 control-label">公司电话</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-mobile"
                                       name="options[mobile]"
                                       value="<?php echo (isset($site_info['mobile']) && ($site_info['mobile'] !== '')?$site_info['mobile']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-address" class="col-sm-2 control-label">公司地址</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-address"
                                       name="options[address]"
                                       value="<?php echo (isset($site_info['address']) && ($site_info['address'] !== '')?$site_info['address']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-apk_ewm" class="col-sm-2 control-label">android版下载二维码</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="hidden" name="options[apk_ewm]" id="thumbnail1" value="<?php echo (isset($site_info['apk_ewm']) && ($site_info['apk_ewm'] !== '')?$site_info['apk_ewm']:''); ?>">
                                <a href="javascript:uploadOneImage('图片上传','#thumbnail1');">
                                    <?php if(empty($site_info['apk_ewm'])): ?>
                                    <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                             id="thumbnail1-preview"
                                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php else: ?>
                                    <img src="<?php echo cmf_get_image_preview_url($site_info['apk_ewm']); ?>"
                                         id="thumbnail1-preview"
                                         style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php endif; ?>
                                </a>
                                <input type="button" class="btn btn-sm btn-cancel-thumbnail1" value="取消图片">  PC首页用
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-ipa_ewm" class="col-sm-2 control-label">iPhone版下载二维码</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="hidden" name="options[ipa_ewm]" id="thumbnail2" value="<?php echo (isset($site_info['ipa_ewm']) && ($site_info['ipa_ewm'] !== '')?$site_info['ipa_ewm']:''); ?>">
                                <a href="javascript:uploadOneImage('图片上传','#thumbnail2');">
                                    <?php if(empty($site_info['ipa_ewm'])): ?>
                                    <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                             id="thumbnail2-preview"
                                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php else: ?>
                                    <img src="<?php echo cmf_get_image_preview_url($site_info['ipa_ewm']); ?>"
                                         id="thumbnail2-preview"
                                         style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php endif; ?>
                                </a>
                                <input type="button" class="btn btn-sm btn-cancel-thumbnail2" value="取消图片">  PC首页用
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-wechat_ewm" class="col-sm-2 control-label">微信公众号</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="hidden" name="options[wechat_ewm]" id="thumbnail3" value="<?php echo (isset($site_info['wechat_ewm']) && ($site_info['wechat_ewm'] !== '')?$site_info['wechat_ewm']:''); ?>">
                                <a href="javascript:uploadOneImage('图片上传','#thumbnail3');">
                                    <?php if(empty($site_info['wechat_ewm'])): ?>
                                    <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                             id="thumbnail3-preview"
                                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php else: ?>
                                    <img src="<?php echo cmf_get_image_preview_url($site_info['wechat_ewm']); ?>"
                                         id="thumbnail3-preview"
                                         style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php endif; ?>
                                </a>
                                <input type="button" class="btn btn-sm btn-cancel-thumbnail3" value="取消图片"> 首页使用 建议尺寸  200 X 200
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-sina_icon" class="col-sm-2 control-label">官微A图标</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="hidden" name="options[sina_icon]" id="thumbnail4" value="<?php echo (isset($site_info['sina_icon']) && ($site_info['sina_icon'] !== '')?$site_info['sina_icon']:''); ?>">
                                <a href="javascript:uploadOneImage('图片上传','#thumbnail4');">
                                    <?php if(empty($site_info['sina_icon'])): ?>
                                    <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                             id="thumbnail4-preview"
                                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php else: ?>
                                    <img src="<?php echo cmf_get_image_preview_url($site_info['sina_icon']); ?>"
                                         id="thumbnail4-preview"
                                         style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php endif; ?>
                                </a>
                                <input type="button" class="btn btn-sm btn-cancel-thumbnail4" value="取消图片">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-sina_title" class="col-sm-2 control-label">官微A标题</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-sina_title"
                                       name="options[sina_title]"
                                       value="<?php echo (isset($site_info['sina_title']) && ($site_info['sina_title'] !== '')?$site_info['sina_title']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-sina_desc" class="col-sm-2 control-label">官微A描述</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-sina_desc"
                                       name="options[sina_desc]"
                                       value="<?php echo (isset($site_info['sina_desc']) && ($site_info['sina_desc'] !== '')?$site_info['sina_desc']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-sina_url" class="col-sm-2 control-label">官微A链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-sina_url"
                                       name="options[sina_url]"
                                       value="<?php echo (isset($site_info['sina_url']) && ($site_info['sina_url'] !== '')?$site_info['sina_url']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-qq_icon" class="col-sm-2 control-label">官微B图标</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="hidden" name="options[qq_icon]" id="thumbnail5" value="<?php echo (isset($site_info['qq_icon']) && ($site_info['qq_icon'] !== '')?$site_info['qq_icon']:''); ?>">
                                <a href="javascript:uploadOneImage('图片上传','#thumbnail5');">
                                    <?php if(empty($site_info['qq_icon'])): ?>
                                    <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                             id="thumbnail5-preview"
                                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php else: ?>
                                    <img src="<?php echo cmf_get_image_preview_url($site_info['qq_icon']); ?>"
                                         id="thumbnail5-preview"
                                         style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php endif; ?>
                                </a>
                                <input type="button" class="btn btn-sm btn-cancel-thumbnail5" value="取消图片">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-qq_title" class="col-sm-2 control-label">官微B标题</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-qq_title"
                                       name="options[qq_title]"
                                       value="<?php echo (isset($site_info['qq_title']) && ($site_info['qq_title'] !== '')?$site_info['qq_title']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-qq_desc" class="col-sm-2 control-label">官微B描述</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-qq_desc"
                                       name="options[qq_desc]"
                                       value="<?php echo (isset($site_info['qq_desc']) && ($site_info['qq_desc'] !== '')?$site_info['qq_desc']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-qq_url" class="col-sm-2 control-label">官微B链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-qq_url"
                                       name="options[qq_url]"
                                       value="<?php echo (isset($site_info['qq_url']) && ($site_info['qq_url'] !== '')?$site_info['qq_url']:''); ?>">以http://或https://开头
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="1">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="B">
                        <div class="form-group">
                            <label for="input-site_seo_title" class="col-sm-2 control-label"><?php echo lang('WEBSITE_SEO_TITLE'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site_seo_title"
                                       name="options[site_seo_title]" value="<?php echo (isset($site_info['site_seo_title']) && ($site_info['site_seo_title'] !== '')?$site_info['site_seo_title']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-site_seo_keywords" class="col-sm-2 control-label"><?php echo lang('WEBSITE_SEO_KEYWORDS'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site_seo_keywords"
                                       name="options[site_seo_keywords]"
                                       value="<?php echo (isset($site_info['site_seo_keywords']) && ($site_info['site_seo_keywords'] !== '')?$site_info['site_seo_keywords']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-site_seo_description" class="col-sm-2 control-label"><?php echo lang('WEBSITE_SEO_DESCRIPTION'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-site_seo_description"
                                          name="options[site_seo_description]"><?php echo (isset($site_info['site_seo_description']) && ($site_info['site_seo_description'] !== '')?$site_info['site_seo_description']:''); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="C">
                        <div class="form-group">
                            <label for="input-isup" class="col-sm-2 control-label">强制更新</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="options[isup]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($site_info['isup'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-apk_ver" class="col-sm-2 control-label">APK版本号</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-apk_ver"
                                       name="options[apk_ver]" value="<?php echo (isset($site_info['apk_ver']) && ($site_info['apk_ver'] !== '')?$site_info['apk_ver']:''); ?>">安卓APP最新的版本号，请勿随意修改
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-apk_url" class="col-sm-2 control-label">APK下载链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-apk_url"
                                       name="options[apk_url]" value="<?php echo (isset($site_info['apk_url']) && ($site_info['apk_url'] !== '')?$site_info['apk_url']:''); ?>">安卓最新版APK下载链接
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-apk_des" class="col-sm-2 control-label">APK更新说明</label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-apk_des"
                                          name="options[apk_des]"><?php echo (isset($site_info['apk_des']) && ($site_info['apk_des'] !== '')?$site_info['apk_des']:''); ?></textarea>APK更新说明（200字以内）
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ipa_ver" class="col-sm-2 control-label">IPA版本号</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ipa_ver"
                                       name="options[ipa_ver]" value="<?php echo (isset($site_info['ipa_ver']) && ($site_info['ipa_ver'] !== '')?$site_info['ipa_ver']:''); ?>">IOS APP最新的版本号，请勿随意修改
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ios_shelves" class="col-sm-2 control-label">IPA上架版本号</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ios_shelves"
                                       name="options[ios_shelves]" value="<?php echo (isset($site_info['ios_shelves']) && ($site_info['ios_shelves'] !== '')?$site_info['ios_shelves']:''); ?>">IOS上架审核中版本的版本号(用于上架期间隐藏上架版本部分功能,不要和IPA版本号相同)
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ipa_url" class="col-sm-2 control-label">IPA下载链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ipa_url"
                                       name="options[ipa_url]" value="<?php echo (isset($site_info['ipa_url']) && ($site_info['ipa_url'] !== '')?$site_info['ipa_url']:''); ?>">IOS最新版IPA下载链接
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ipa_des" class="col-sm-2 control-label">IPA更新说明</label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-ipa_des"
                                          name="options[ipa_des]"><?php echo (isset($site_info['ipa_des']) && ($site_info['ipa_des'] !== '')?$site_info['ipa_des']:''); ?></textarea>IPA更新说明（200字以内）
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-qr_url" class="col-sm-2 control-label">二维码下载链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="hidden" name="options[qr_url]" id="thumbnail6" value="<?php echo (isset($site_info['qr_url']) && ($site_info['qr_url'] !== '')?$site_info['qr_url']:''); ?>">
                                <a href="javascript:uploadOneImage('图片上传','#thumbnail6');">
                                    <?php if(empty($site_info['qr_url'])): ?>
                                    <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                             id="thumbnail6-preview"
                                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php else: ?>
                                    <img src="<?php echo cmf_get_image_preview_url($site_info['qr_url']); ?>"
                                         id="thumbnail6-preview"
                                         style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php endif; ?>
                                </a>
                                <input type="button" class="btn btn-sm btn-cancel-thumbnail6" value="取消图片">
                                PC下载页面用 二维码生成链接：<?php echo $site_info['site']; ?>/Portal/index/scanqr    
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="D">
                        <div class="form-group">
                            <label for="input-site-name" class="col-sm-2 control-label">登录方式</label>
                            <div class="col-md-6 col-sm-10">
                                <?php 
									$qq='qq';
									$wx='wx';
									$sina='sina';
									$facebook='facebook';
									$twitter='twitter';
								 ?>
								<label class="checkbox-inline"><input type="checkbox" value="qq" name="login_type[]" <?php if(in_array(($qq), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>QQ</label>
								<label class="checkbox-inline"><input type="checkbox" value="wx" name="login_type[]" <?php if(in_array(($wx), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>微信</label>
								<label class="checkbox-inline"><input type="checkbox" value="facebook" name="login_type[]" <?php if(in_array(($facebook), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>FaceBook</label>
								<label class="checkbox-inline"><input type="checkbox" value="twitter" name="login_type[]" <?php if(in_array(($twitter), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>Twitter</label>
								
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-site-name" class="col-sm-2 control-label">分享方式</label>
                            <div class="col-md-6 col-sm-10">
                                <?php 
                                    $share_qq='qq';
									$share_qzone='qzone';
									$share_wx='wx';
									$share_wchat='wchat';
									$share_sina='sina';
									$share_facebook='facebook';
									$share_twitter='twitter';
								 ?>
								<label class="checkbox-inline"><input type="checkbox" value="qq" name="share_type[]" <?php if(in_array(($share_qq), is_array($site_info['share_type'])?$site_info['share_type']:explode(',',$site_info['share_type']))): ?>checked="checked"<?php endif; ?>>QQ</label>
								<label class="checkbox-inline"><input type="checkbox" value="qzone" name="share_type[]" <?php if(in_array(($share_qzone), is_array($site_info['share_type'])?$site_info['share_type']:explode(',',$site_info['share_type']))): ?>checked="checked"<?php endif; ?>>QQ空间</label>
								<label class="checkbox-inline"><input type="checkbox" value="wx" name="share_type[]" <?php if(in_array(($share_wx), is_array($site_info['share_type'])?$site_info['share_type']:explode(',',$site_info['share_type']))): ?>checked="checked"<?php endif; ?>>微信</label>
								<label class="checkbox-inline"><input type="checkbox" value="wchat" name="share_type[]" <?php if(in_array(($share_wchat), is_array($site_info['share_type'])?$site_info['share_type']:explode(',',$site_info['share_type']))): ?>checked="checked"<?php endif; ?>>微信朋友圈</label>
								<label class="checkbox-inline"><input type="checkbox" value="facebook" name="share_type[]" <?php if(in_array(($share_facebook), is_array($site_info['share_type'])?$site_info['share_type']:explode(',',$site_info['share_type']))): ?>checked="checked"<?php endif; ?>>FaceBook</label>
								<label class="checkbox-inline"><input type="checkbox" value="twitter" name="share_type[]" <?php if(in_array(($share_twitter), is_array($site_info['share_type'])?$site_info['share_type']:explode(',',$site_info['share_type']))): ?>checked="checked"<?php endif; ?>>Twitter</label>
								
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="E">
                        <div class="form-group">
                            <label for="input-wx_siteurl" class="col-sm-2 control-label">微信推广域名</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-wx_siteurl"
                                       name="options[wx_siteurl]" value="<?php echo (isset($site_info['wx_siteurl']) && ($site_info['wx_siteurl'] !== '')?$site_info['wx_siteurl']:''); ?>">
                                       http:// 开头 参数值为用户ID
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-share_title" class="col-sm-2 control-label">直播分享标题</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-share_title"
                                       name="options[share_title]" value="<?php echo (isset($site_info['share_title']) && ($site_info['share_title'] !== '')?$site_info['share_title']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-share_des" class="col-sm-2 control-label">直播分享话术</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-share_des"
                                       name="options[share_des]" value="<?php echo (isset($site_info['share_des']) && ($site_info['share_des'] !== '')?$site_info['share_des']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-app_android" class="col-sm-2 control-label">AndroidAPP下载链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-app_android"
                                       name="options[app_android]" value="<?php echo (isset($site_info['app_android']) && ($site_info['app_android'] !== '')?$site_info['app_android']:''); ?>">
                                分享用Android APP 下载链接
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-app_ios" class="col-sm-2 control-label">IOSAPP下载链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-app_ios"
                                       name="options[app_ios]" value="<?php echo (isset($site_info['app_ios']) && ($site_info['app_ios'] !== '')?$site_info['app_ios']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-video_share_title" class="col-sm-2 control-label">短视频分享标题</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-video_share_title"
                                       name="options[video_share_title]" value="<?php echo (isset($site_info['video_share_title']) && ($site_info['video_share_title'] !== '')?$site_info['video_share_title']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-video_share_des" class="col-sm-2 control-label">短视频分享话术</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-video_share_des"
                                       name="options[video_share_des]" value="<?php echo (isset($site_info['video_share_des']) && ($site_info['video_share_des'] !== '')?$site_info['video_share_des']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="H">
                        <div class="form-group">
                            <label for="input-site-name" class="col-sm-2 control-label">房间类型</label>
                            <div class="col-md-6 col-sm-10">
                                <?php 
									$type_0='0;普通房间';
									$type_1='1;密码房间';
									$type_2='2;门票房间';
									$type_3='3;计时房间';
								 ?>
                                <label class="checkbox-inline hide"><input type="checkbox" value="0;普通房间" name="live_type[]" checked="checked">普通房间</label>
								<label class="checkbox-inline"><input type="checkbox" value="1;密码房间" name="live_type[]" <?php if(in_array(($type_1), is_array($site_info['live_type'])?$site_info['live_type']:explode(',',$site_info['live_type']))): ?>checked="checked"<?php endif; ?>>密码房间</label>
								<label class="checkbox-inline"><input type="checkbox" value="2;门票房间" name="live_type[]" <?php if(in_array(($type_2), is_array($site_info['live_type'])?$site_info['live_type']:explode(',',$site_info['live_type']))): ?>checked="checked"<?php endif; ?>>门票房间</label>
								<label class="checkbox-inline"><input type="checkbox" value="3;计时房间" name="live_type[]" <?php if(in_array(($type_3), is_array($site_info['live_type'])?$site_info['live_type']:explode(',',$site_info['live_type']))): ?>checked="checked"<?php endif; ?>>计时房间</label>
								
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-live_time_coin" class="col-sm-2 control-label">计时直播收费</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-live_time_coin"
                                       name="options[live_time_coin]" value="<?php echo (isset($site_info['live_time_coin']) && ($site_info['live_time_coin'] !== '')?$site_info['live_time_coin']:''); ?>">
                                       计时直播收费，价格梯度用 , 分割 例：1,2,3
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="I">
                        <div class="form-group">
                            <label for="input-sprout_key" class="col-sm-2 control-label">萌颜授权码-Andriod</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-sprout_key"
                                       name="options[sprout_key]" value="<?php echo (isset($site_info['sprout_key']) && ($site_info['sprout_key'] !== '')?$site_info['sprout_key']:''); ?>">
                                       留空 表示使用默认美颜
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-sprout_key_ios" class="col-sm-2 control-label">萌颜授权码-IOS</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-sprout_key_ios"
                                       name="options[sprout_key_ios]" value="<?php echo (isset($site_info['sprout_key_ios']) && ($site_info['sprout_key_ios'] !== '')?$site_info['sprout_key_ios']:''); ?>">
                                       留空 表示使用默认美颜
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-sprout_white" class="col-sm-2 control-label">美颜</label>
                            <div class="col-md-6 col-sm-10">
                                美白：<input type="text" class="form-control control3" name="options[skin_whiting]" value="<?php echo (isset($site_info['skin_whiting']) && ($site_info['skin_whiting'] !== '')?$site_info['skin_whiting']:'-1'); ?>" > <br><br>
                                磨皮：<input type="text" class="form-control control3" name="options[skin_smooth]" value="<?php echo (isset($site_info['skin_smooth']) && ($site_info['skin_smooth'] !== '')?$site_info['skin_smooth']:'-1'); ?>" > <br><br>
                                红润：<input type="text" class="form-control control3" name="options[skin_tenderness]" value="<?php echo (isset($site_info['skin_tenderness']) && ($site_info['skin_tenderness'] !== '')?$site_info['skin_tenderness']:'-1'); ?>" > <br><br>
                                0-9 整数 -1为不设置
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-sprout_skin" class="col-sm-2 control-label">磨皮默认值</label>
                            <div class="col-md-6 col-sm-10">
                                眉毛：<input type="text" class="form-control control3" name="options[eye_brow]" value="<?php echo (isset($site_info['eye_brow']) && ($site_info['eye_brow'] !== '')?$site_info['eye_brow']:'-1'); ?>" > <br><br>
                                大眼：<input type="text" class="form-control control3" name="options[big_eye]" value="<?php echo (isset($site_info['big_eye']) && ($site_info['big_eye'] !== '')?$site_info['big_eye']:'-1'); ?>" > <br><br>
                                眼距：<input type="text" class="form-control control3" name="options[eye_length]" value="<?php echo (isset($site_info['eye_length']) && ($site_info['eye_length'] !== '')?$site_info['eye_length']:'-1'); ?>" > <br><br>
                                眼角：<input type="text" class="form-control control3" name="options[eye_corner]" value="<?php echo (isset($site_info['eye_corner']) && ($site_info['eye_corner'] !== '')?$site_info['eye_corner']:'-1'); ?>" > <br><br>
                                开眼角：<input type="text" class="form-control control3" name="options[eye_alat]" value="<?php echo (isset($site_info['eye_alat']) && ($site_info['eye_alat'] !== '')?$site_info['eye_alat']:'-1'); ?>" > <br><br>
                                瘦脸：<input type="text" class="form-control control3" name="options[face_lift]" value="<?php echo (isset($site_info['face_lift']) && ($site_info['face_lift'] !== '')?$site_info['face_lift']:'-1'); ?>" > <br><br>
                                削脸：<input type="text" class="form-control control3" name="options[face_shave]" value="<?php echo (isset($site_info['face_shave']) && ($site_info['face_shave'] !== '')?$site_info['face_shave']:'-1'); ?>" > <br><br>
                                嘴形：<input type="text" class="form-control control3" name="options[mouse_lift]" value="<?php echo (isset($site_info['mouse_lift']) && ($site_info['mouse_lift'] !== '')?$site_info['mouse_lift']:'-1'); ?>" > <br><br>
                                瘦鼻：<input type="text" class="form-control control3" name="options[nose_lift]" value="<?php echo (isset($site_info['nose_lift']) && ($site_info['nose_lift'] !== '')?$site_info['nose_lift']:'-1'); ?>" > <br><br>
                                下巴：<input type="text" class="form-control control3" name="options[chin_lift]" value="<?php echo (isset($site_info['chin_lift']) && ($site_info['chin_lift'] !== '')?$site_info['chin_lift']:'-1'); ?>" > <br><br>
                                额头：<input type="text" class="form-control control3" name="options[forehead_lift]" value="<?php echo (isset($site_info['forehead_lift']) && ($site_info['forehead_lift'] !== '')?$site_info['forehead_lift']:'-1'); ?>" > <br><br>
                                长鼻：<input type="text" class="form-control control3" name="options[lengthen_noseLift]" value="<?php echo (isset($site_info['lengthen_noseLift']) && ($site_info['lengthen_noseLift'] !== '')?$site_info['lengthen_noseLift']:'-1'); ?>" > <br><br>
                                0-100 整数 -1为不设置
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="G">
                        <div class="form-group">
                            <label for="input-cdn_static_root" class="col-sm-2 control-label">静态资源cdn地址</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-cdn_static_root"
                                       name="cdn_settings[cdn_static_root]"
                                       value="<?php echo (isset($cdn_settings['cdn_static_root']) && ($cdn_settings['cdn_static_root'] !== '')?$cdn_settings['cdn_static_root']:''); ?>">
                                <p class="help-block">
                                    不能以/结尾；设置这个地址后，请将ThinkCMF下的静态资源文件放在其下面；<br>
                                    ThinkCMF下的静态资源文件大致包含以下(如果你自定义后，请自行增加)：<br>
                                    themes/admin_simplebootx/public/assets<br>
                                    static<br>
                                    themes/simplebootx/public/assets<br>
                                    例如未设置cdn前：jquery的访问地址是/static/js/jquery.js, <br>
                                    设置cdn是后它的访问地址就是：静态资源cdn地址/static/js/jquery.js
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>

</div>
<script type="text/javascript" src="/static/js/admin.js"></script>
<script type="text/javascript">
    (function(){
        $('.btn-cancel-thumbnail1').click(function () {
            $('#thumbnail1-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
            $('#thumbnail1').val('');
        });
        
        $('.btn-cancel-thumbnail2').click(function () {
            $('#thumbnail2-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
            $('#thumbnail2').val('');
        });
        
        $('.btn-cancel-thumbnail3').click(function () {
            $('#thumbnail3-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
            $('#thumbnail3').val('');
        });
        
        $('.btn-cancel-thumbnail4').click(function () {
            $('#thumbnail4-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
            $('#thumbnail4').val('');
        });
        
        $('.btn-cancel-thumbnail5').click(function () {
            $('#thumbnail5-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
            $('#thumbnail5').val('');
        });
        
        $('.btn-cancel-thumbnail6').click(function () {
            $('#thumbnail6-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
            $('#thumbnail6').val('');
        });
        
    })()

</script>
</body>
</html>
