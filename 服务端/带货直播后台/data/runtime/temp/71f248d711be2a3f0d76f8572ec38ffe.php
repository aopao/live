<?php /*a:0:{}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/detail.css?t=1540544885"/> 
    <title>我的明细</title>
</head>
<body class="detail">
	<div class="profit_bg">
		<div class="tab">
			<ul>
				<li class="on">
                    收礼物明细
                    <div class="tab_line bg_default"></div>
                </li>
				<li>
                    直播时长明细
                    <div class="tab_line bg_default"></div>
                </li>
			</ul>
		</div>
        <div class="profit_line"></div>
		<div class="tab_b receive">
			<div class="profit_ul_t">
				<span class="name">礼物名称</span>
				<span class="coin">价值</span>
				<span class="nums">个数</span>
				<span class="username">送礼人</span>
			</div>
			<div class="profit_ul clear">
				<ul>
									</ul>
			</div>
		</div>
		<div class="tab_b hide liverecord">
			<div class="profit_ul_t">
				<span style="width:36%;">开始时间</span>
				<span style="width:36%;">结束时间</span>
				<span style="width:28%;">直播时长</span>
			</div>
			<div class="profit_ul clear">
				<ul>
									</ul>
			</div>
		</div>
	</div>
	<script>
    var uid='37128';
    var token='bfbf74644aa9e9cc5cc619bfd11d9e08';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


	<script>
	$(function(){
		function getlistmore(){
			$.ajax({
				url:'/appapi/detail/receive_more',
				data:{'page':page,'uid':uid,'token':token},
				type:'post',
				dataType:'json',
				success:function(data){
					if(data.nums>0){
							var nums=data.nums;
							var list=data.data;
							var html='';
							for(var i=0;i<nums;i++){
								html='<li>\
										<span>'+list[i]['giftinfo']['giftname']+'</span>\
										<span>'+list[i]['totalall']+'</span>\
										<span>'+list[i]['num']+'</span>\
										<span>'+list[i]['userinfo']['user_nicename']+'</span>\
									</li>';
							}
						
						$(".receive .profit_ul ul").append(html);
					}
					
					if(data.isscroll==1){
						page++;
						isscroll=true;
					}
				}
			})
		}
		
		function getlivelistmore(){
			$.ajax({
				url:'/appapi/detail/liverecord_more',
				data:{'page':page,'uid':uid,'token':token},
				type:'post',
				dataType:'json',
				success:function(data){
					if(data.nums>0){
							var nums=data.nums;
							var list=data.data;
							var html='';
							for(var i=0;i<nums;i++){
								html='<li>\
										<span style="width:35%;">'+list[i]['starttime']+'</span>\
										<span style="width:35%;">'+list[i]['endtime']+'</span>\
										<span style="width:30%;">'+list[i]['length']+'</span>\
									</li>';
							}
						
						$(".liverecord .profit_ul ul").append(html);
					}
					
					if(data.isscroll==1){
						page++;
						isscroll2=true;
					}
				}
			})
		}
		
		$(".tab ul li").on("click",function(){
			$(this).siblings().removeClass("on");
			$(this).addClass("on");
			$(".tab_b").hide().eq($(this).index()).show();
		})
		var page=2; 
		var isscroll=true; 
		var isscroll2=true; 

		$(".receive .profit_ul").scroll(function(){  
				var srollPos = $(".receive .profit_ul").scrollTop();    //滚动条距顶部距离(页面超出窗口的高度)  		
				var totalheight = parseFloat($(".receive .profit_ul").height()) + parseFloat(srollPos);  
				if(($(document).height()-50) <= totalheight  && isscroll) {  
						isscroll=false;
						getlistmore()
				}  
		});  
		
		$(".liverecord .profit_ul").scroll(function(){  
				var srollPos = $(".liverecord .profit_ul").scrollTop();    //滚动条距顶部距离(页面超出窗口的高度)  		
				var totalheight = parseFloat($(".liverecord .profit_ul").height()) + parseFloat(srollPos);  
				if(($(document).height()-50) <= totalheight  && isscroll2) {  
						isscroll2=false;
						getlivelistmore()
				}  
		});  

	})
	</script>	
</body>
</html>