<?php /*a:2:{s:76:"/www/wwwroot/zhibo.daweia.cn/themes/admin_simpleboot3/admin/family/edit.html";i:1579317638;s:72:"/www/wwwroot/zhibo.daweia.cn/themes/admin_simpleboot3/public/header.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta name="referrer" content="origin">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li ><a href="<?php echo url('Family/index'); ?>">列表</a></li>
			<li class="active"><a ><?php echo lang('EDIT'); ?></a></li>
		</ul>
		<form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('Family/editPost'); ?>">
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>家族名称</label>
				<div class="col-md-6 col-sm-10" style="padding-top:7px;">
					<?php echo $data['name']; ?>
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>家族长</label>
				<div class="col-md-6 col-sm-10" style="padding-top:7px;">
					<?php echo $data['userinfo']['user_nicename']; ?> (<?php echo $data['uid']; ?>)
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-votes" class="col-sm-2 control-label"><span class="form-required">*</span>家族简介</label>
				<div class="col-md-6 col-sm-10" style="padding-top:7px;">
					<?php echo $data['briefing']; ?>
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-money" class="col-sm-2 control-label"><span class="form-required">*</span>家族徽章</label>
				<div class="col-md-6 col-sm-10" style="padding-top:7px;">
					<img src="<?php echo $data['badge']; ?>" style="cursor: hand;max-width:100px;max-height:200px;" />
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-money" class="col-sm-2 control-label"><span class="form-required">*</span>身份证正面</label>
				<div class="col-md-6 col-sm-10" style="padding-top:7px;">
					<img src="<?php echo $data['apply_pos']; ?>" style="cursor: hand;max-width:100px;max-height:200px;" />
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-money" class="col-sm-2 control-label"><span class="form-required">*</span>身份证反面</label>
				<div class="col-md-6 col-sm-10" style="padding-top:7px;">
					<img src="<?php echo $data['apply_side']; ?>" style="cursor: hand;max-width:100px;max-height:200px;" />
				</div>
			</div>
            
            
            <div class="form-group">
				<label for="input-divide_family" class="col-sm-2 control-label"><span class="form-required">*</span>家族抽成</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-divide_family" name="divide_family" value="<?php echo $data['divide_family']; ?>">
				</div>
			</div>
            
            
            
            <div class="form-group">
				<label for="input-trade_no" class="col-sm-2 control-label"><span class="form-required">*</span>审核</label>
				<div class="col-md-6 col-sm-10">
					<select class="form-control" name="state">
                        <?php if(is_array($state) || $state instanceof \think\Collection || $state instanceof \think\Paginator): $i = 0; $__LIST__ = $state;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                        <option value="<?php echo $key; ?>" <?php if($data['state'] == $key): ?>selected<?php endif; ?>><?php echo $v; ?></option>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-reason" class="col-sm-2 control-label"><span class="form-required">*</span>原因</label>
				<div class="col-md-6 col-sm-10">
					<textarea class="form-control" id="input-reason" name="reason" ><?php echo (isset($data['reason']) && ($data['reason'] !== '')?$data['reason']:''); ?></textarea>
				</div>
			</div>
            

            <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
					<button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('EDIT'); ?></button>
					<a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
				</div>
			</div>

		</form>
	</div>
	<script src="/static/js/admin.js"></script>
</body>
</html>