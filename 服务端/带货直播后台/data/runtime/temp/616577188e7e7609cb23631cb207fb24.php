<?php /*a:3:{s:69:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/family/index2.html";i:1579317638;s:60:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/head.html";i:1579317638;s:62:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/footer.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/family.css?t=1561712925"/>
    <title>家族中心</title>
</head>
<body class="attended">
	<div class="search">
		<div class="search_input">
			<input id="key" placeholder="搜索签约家族ID/名称">
			<span class="search_clear"></span>
		</div>
		<div class="search_btn color_default">
			搜索
		</div>
	</div>
	<div class="line10"></div>
	<div class="list_title">
		家族列表
		<div class="reload"></div>
	</div>
	<div class="user-list user-list-fillet">
		<ul>
			<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $k = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?>
			<li>
				<a href="/Appapi/Family/detail?familyid=<?php echo $vo['id']; ?>&uid=<?php echo $uid; ?>&token=<?php echo $token; ?>">
					<div class="thumb">
						<img src="<?php echo $vo['badge']; ?>">
					</div>
					<div class="info">
						<p class="info-title"><?php echo $vo['name']; ?></p>
						<p class="info-des2 ellipsis"><?php echo $vo['briefing']; ?></p>
						<p class="info-des2"><span>成员：<?php echo $vo['count']; ?>人</span><span>ID：<?php echo $vo['id']; ?></span></p>
					</div>
					<div class="action">
						<span class="ok" data-familyid="<?php echo $vo['id']; ?>">申请</span>
					</div>
				</a>
			</li>
			<?php endforeach; endif; else: echo "" ;endif; ?>
		</ul>
	</div>
    
	<script>
    var uid='<?php echo (isset($uid) && ($uid !== '')?$uid:''); ?>';
    var token='<?php echo (isset($token) && ($token !== '')?$token:''); ?>';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


	<script src="/static/appapi/js/family.js"></script>
		
</body>
</html>