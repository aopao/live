<?php /*a:3:{s:66:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/shop/index.html";i:1579317638;s:60:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/head.html";i:1579317638;s:62:"/www/wwwroot/zhibo.daweia.cn/themes/default/appapi/footer.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/shop.css"/> 
    <title>我的小店</title>
</head>
<body>
    <div class="ready">
        <div class="ready_top">
            <p class="ready_top_t">申请条件</p>
            <p class="ready_top_d">完成以下条件即可申请店铺</p>
        </div>
        <div class="ready_info">
            <ul>
                <li>
                    <div class="li_img">
                        <img src="/static/appapi/images/shop/fans.png">
                    </div>
                    <div class="li_info">
                        <p class="li_info_t">粉丝数量</p>
                        <p class="li_info_d">>=<?php echo $shop_fans; ?>名</p>
                    </div>
                    
                    <div class="li_btn">
                        <?php if($fans_ok == 1): ?>
                        <span class="ok">已达成</span>
                        <?php else: ?>
                        <span class="fans_no">未达成</span>
                        <?php endif; ?>
                    </div>
                </li>
                
                <li>
                    <div class="li_img">
                        <img src="/static/appapi/images/shop/level.png">
                    </div>
                    <div class="li_info">
                        <p class="li_info_t">主播等级</p>
                        <p class="li_info_d">>=Lv.<?php echo $shop_level; ?></p>
                    </div>
                    
                    <div class="li_btn">
                        <?php if($level_ok == 1): ?>
                        <span class="ok">已达成</span>
                        <?php else: ?>
                        <!-- <span class="no">去完成</span> -->
                        <span class="fans_no">未达成</span>
                        <?php endif; ?>
                    </div>
                </li>
                
                <li>
                    <div class="li_img">
                        <img src="/static/appapi/images/shop/bond.png">
                    </div>
                    <div class="li_info">
                        <p class="li_info_t">缴纳保证金</p>
                        <p class="li_info_d"><?php echo $shop_bond; ?><img src="/static/appapi/images/coin.png" class="coin"></p>
                    </div>
                    
                    <div class="li_btn">
                        <?php if($bond_ok == 1): ?>
                        <span class="ok">已达成</span>
                        <?php else: ?>
                        <!-- <span class="no">去完成</span> -->
                        <a class="fans_no bond_btn" href="/appapi/shop/bond?uid=<?php echo $uid; ?>&token=<?php echo $token; ?>">去缴纳</a>
                        <?php endif; ?>
                        
                    </div>
                </li>
            </ul>
        </div>
        <div class="ready_tips">
            <!-- <div class="ready_tips_t">获得权益</div> -->
            <div class="ready_tips_d">店铺创建完成您将获得以下权益</div>
            <div class="ready_tips_d2">
                <p>1. 个人主页拥有我的小店橱窗入口</p>
                <p>2. 在直播间内添加直播购物车</p>
                <p>3. 发布短视频作品时可关联商品</p>
            </div>
        </div>
        <div class="ready_apply">
            <a <?php if($isapply == '1'): ?>href="/appapi/shop/apply?uid=<?php echo $uid; ?>&token=<?php echo $token; ?>&reset=1" class="on"<?php endif; ?> >立即申请</a>
        </div>

    </div>
	<script>
    var uid='<?php echo (isset($uid) && ($uid !== '')?$uid:''); ?>';
    var token='<?php echo (isset($token) && ($token !== '')?$token:''); ?>';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


</body>
</html>