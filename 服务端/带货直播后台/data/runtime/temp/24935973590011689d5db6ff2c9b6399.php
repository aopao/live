<?php /*a:2:{s:75:"/www/wwwroot/zhibo.daweia.cn/themes/admin_simpleboot3/admin/music/edit.html";i:1579317638;s:72:"/www/wwwroot/zhibo.daweia.cn/themes/admin_simpleboot3/public/header.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta name="referrer" content="origin">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li ><a href="<?php echo url('Music/index'); ?>">列表</a></li>
			<li class="active"><a ><?php echo lang('EDIT'); ?></a></li>
		</ul>
		<form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('Music/editPost'); ?>">
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>分类</label>
				<div class="col-md-6 col-sm-10">
					<select class="form-control" name="classify_id">
                        <?php if(is_array($classify) || $classify instanceof \think\Collection || $classify instanceof \think\Paginator): $i = 0; $__LIST__ = $classify;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                        <option value="<?php echo $key; ?>" <?php if($data['classify_id'] == $key): ?>selected<?php endif; ?>><?php echo $v; ?></option>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-title" class="col-sm-2 control-label"><span class="form-required">*</span>名称</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-title" name="title" value="<?php echo $data['title']; ?>">
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-author" class="col-sm-2 control-label"><span class="form-required">*</span>演唱者</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-author" name="author" value="<?php echo $data['author']; ?>">
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label"><span class="form-required">*</span>封面</label>
				<div class="col-md-6 col-sm-10">
					<input type="hidden" name="img_url" id="thumbnail" value="<?php echo $data['img_url']; ?>">
                    <a href="javascript:uploadOneImage('图片上传','#thumbnail');">
                        <?php if(empty($data['img_url'])): ?>
                        <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                 id="thumbnail-preview"
                                 style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php else: ?>
                        <img src="<?php echo cmf_get_image_preview_url($data['img_url']); ?>"
                             id="thumbnail-preview"
                             style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php endif; ?>
                    </a>
                    <input type="button" class="btn btn-sm btn-cancel-thumbnail" value="取消图片"> 建议尺寸： 200 X 200
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-words" class="col-sm-2 control-label"><span class="form-required">*</span>上传音乐</label>
				<div class="col-md-6 col-sm-10">
                    <input class="form-control" type="file" name="file" id="upfile" style="width: 300px;display: inline-block;"/>
					<!-- <input class="form-control" id="js-file-input" type="text" name="file_url" value="<?php echo $data['file_url']; ?>" style="width: 300px;display: inline-block;" title="文件名称">
                        <a href="javascript:uploadOne('文件上传','#js-file-input','audio');">上传文件</a> -->MP3格式
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-length" class="col-sm-2 control-label"><span class="form-required">*</span>音乐长度</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-length" name="length" value="<?php echo $data['length']; ?>" style="width: 300px;display: inline-block;" readonly> 上传时自动获取
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-use_nums" class="col-sm-2 control-label"><span class="form-required">*</span>被使用次数</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-use_nums" name="use_nums" value="<?php echo $data['use_nums']; ?>">
				</div>
			</div>

            <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
					<button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('EDIT'); ?></button>
					<a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
				</div>
			</div>

		</form>
	</div>
	<script src="/static/js/admin.js"></script>
    <script type="text/javascript">
        (function(){
            $('.btn-cancel-thumbnail').click(function () {
                $('#thumbnail-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
                $('#thumbnail').val('');
            });
        })()
        
        $(function () {  
            $("#upfile").change(function () {
            	//获取文件类型
            	var a=$("#upfile").val();
            	var arr=a.split('.');
            	var type=arr[arr.length-1];
            	if(type.toLowerCase()!="mp3"){
            		layer.msg("请上传MP3格式文件");
            		$("#submit").attr("disabled","true");
            		return;
            	}


                var objUrl = getObjectURL(this.files[0]);  
                $("#audio").attr("src", objUrl);  
                //$("#audio")[0].play();  
                $("#audio").show();  
                getTime();  
            });  
        }); 


        //获取mp3文件的时间 兼容浏览器  
        function getTime() {   
            setTimeout(function () {  
                var duration = $("#audio")[0].duration;  
                if(isNaN(duration)){  
                    //layer.msg("文件错误，请重新选择");
                    getTime();
                    return !1;
                }  
                //console.log($("#audio")[0]); 
               // console.info("该歌曲的总时间为："+$("#audio")[0].duration+"秒");
                var length=Math.floor($("#audio")[0].duration); //获取音乐长度

                if(length<15){ //长度小于15秒
                    layer.msg('音乐长度不能低于15秒');
                }

                var len_str="00:00";
                if(length>60){
                    var minute=Math.floor(length/60);
                    var second=length%60;
                    if(minute<10){
                        minute="0"+minute;
                    }
                    if(second<10){
                        second="0"+second;
                    }
                    len_str=minute+":"+second;

                }else{
                    if(length<10){
                        length="0"+length;
                    }
                    len_str="00:"+length;
                }

                //console.log(len_str);
                $("#input-length").val(len_str);
            }, 50); 
        }  
        //把文件转换成可读URL 
        function getObjectURL(file) {

            var url = null;  
            if (window.createObjectURL != undefined) { // basic  
                url = window.createObjectURL(file);  
            } else if (window.URL != undefined) { // mozilla(firefox)  
                url = window.URL.createObjectURL(file);  
            } else if (window.webkitURL != undefined) { // webkit or chrome  
                url = window.webkitURL.createObjectURL(file);  
            }  
            return url;  
        }

    </script>
</body>
</html>