<?php /*a:0:{}*/ ?>
<!DOCTYPE html>
<html>
<head lang="en">
    
    <meta charset="utf-8">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="telephone=no" name="format-detection" />
    <link href='/static/appapi/css/common.css?t=1576565542' rel="stylesheet" type="text/css" >

	<link type="text/css" rel="stylesheet" href="/static/appapi/css/family.css?t=1561712925"/>
    <title>申请进度</title>
</head>
<body>
	<div class="apply_ok">
		<div class="speed">
			<div class="speed_thumb"><img src="/static/appapi/images/family/create_ok.png"></div>
			<div class="speed_title">恭喜您，家族创建成功</div>
		</div>
		<a class="reset button_default" href="/Appapi/Family/home?uid=37104&token=5b237e5f68dc93f6534413c1fd5db001&reset=2">进入我的家族</a>
	</div>
    <script>
    var uid='37104';
    var token='5b237e5f68dc93f6534413c1fd5db001';
    var baseSize = 100;
    function setRem () {
      var scale = document.documentElement.clientWidth / 750;
      document.documentElement.style.fontSize = (baseSize * Math.min(scale, 3)) + 'px';
    }
    setRem();
    window.onresize = function () {
      setRem();
    }
</script>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/layer/layer.js"></script>


</body>
</html>