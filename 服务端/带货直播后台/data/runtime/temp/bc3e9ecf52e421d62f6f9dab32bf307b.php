<?php /*a:2:{s:80:"/www/wwwroot/zhibo.daweia.cn/themes/admin_simpleboot3/admin/shopgoods/index.html";i:1579317638;s:72:"/www/wwwroot/zhibo.daweia.cn/themes/admin_simpleboot3/public/header.html";i:1579317638;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta name="referrer" content="origin">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a >列表</a></li>

		</ul>
		<form class="well form-inline margin-top-20" method="post" action="<?php echo url('Shopgoods/index'); ?>">
            价格：
            <input class="form-control" name="start_coin" id="start_coin" value="<?php echo input('request.start_coin'); ?>" style="width: 110px;"> - 
            <input class="form-control" name="end_coin" id="end_coin" value="<?php echo input('request.end_coin'); ?>" style="width: 110px;">
			提交时间：
			<input class="form-control js-bootstrap-date" name="start_time" id="start_time" value="<?php echo input('request.start_time'); ?>" aria-invalid="false" style="width: 110px;"> - 
            <input class="form-control js-bootstrap-date" name="end_time" id="end_time" value="<?php echo input('request.end_time'); ?>" aria-invalid="false" style="width: 110px;">
			用户： 
            <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
                   placeholder="请输入会员ID、靓号">
            名称： 
            <input class="form-control" type="text" name="keyword" style="width: 200px;" value="<?php echo input('request.keyword'); ?>"
                   placeholder="请输入商品名称">
			<input type="submit" class="btn btn-primary" value="搜索">
		</form>				
		<form method="post" class="js-ajax-form" >

			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>会员</th>
						<th width="10%">名称</th>
						<th>封面</th>
						<th>原价</th>
						<th>现价</th>
						<th width="20%">描述</th>
						<th>类型</th>
						<th width="20%">链接</th>
						<th>状态</th>
						<th>提交时间</th>
						<th><?php echo lang('ACTIONS'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(is_array($lists) || $lists instanceof \think\Collection || $lists instanceof \think\Paginator): if( count($lists)==0 ) : echo "" ;else: foreach($lists as $key=>$vo): ?>
					<tr>
						<td><?php echo $vo['id']; ?></td>
						<td><?php echo $vo['userinfo']['user_nicename']; ?> (<?php echo $vo['uid']; ?>) </td>
						<td><?php echo $vo['name']; ?></td>
						<td><img src="<?php echo $vo['thumb']; ?>" style="width:100px;"></td>
						<td><?php echo $vo['old_price']; ?></td>
						<td><?php echo $vo['price']; ?></td>
						<td><?php echo $vo['des']; ?></td>
						<td><?php echo $type[$vo['type']]; ?></td>
						<td style="word-break: break-all;"><?php echo $vo['href']; ?></td>
                        <td><?php echo $status[$vo['status']]; ?></td>
						<td><?php echo date('Y-m-d H:i:s',$vo['addtime']); ?></td>
						<td>	
                            <?php if($vo['status'] == -2): ?>
                            <a class="btn btn-xs btn-info js-ajax-dialog-btn" href="<?php echo url('Shopgoods/setstatus',array('id'=>$vo['id'],'status'=>'1')); ?>" >取消下架</a>
                            <?php elseif($vo['status'] == 1): ?>
                            <a class="btn btn-xs btn-info js-ajax-dialog-btn" href="<?php echo url('Shopgoods/setstatus',array('id'=>$vo['id'],'status'=>'-2')); ?>" >下架</a>
                            <?php endif; ?>
                            <!-- <?php if($vo['isrecom'] == 1): ?>
                            <a class="btn btn-xs btn-info js-ajax-dialog-btn" href="<?php echo url('Shopgoods/setRecom',array('id'=>$vo['id'],'isrecom'=>'0')); ?>" >取消推荐</a>
                            <?php else: ?>
                            <a class="btn btn-xs btn-info js-ajax-dialog-btn" href="<?php echo url('Shopgoods/setRecom',array('id'=>$vo['id'],'isrecom'=>'1')); ?>" >推荐</a>
                            <?php endif; ?> -->
                            <a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('Shopgoods/del',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
						</td>
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="pagination"><?php echo $page; ?></div>
		</form>
	</div>
	<script src="/static/js/admin.js"></script>
</body>
</html>